// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//
use itertools::Itertools;
use std::collections::{BTreeMap, HashMap};

use k8s::openapi::api::apps::v1::DaemonSet;
use k8s::openapi::api::apps::v1::DaemonSetSpec;
use k8s::openapi::api::apps::v1::DaemonSetUpdateStrategy;
use k8s::openapi::api::apps::v1::Deployment;
use k8s::openapi::api::apps::v1::DeploymentSpec;
use k8s::openapi::api::apps::v1::DeploymentStrategy;
use k8s::openapi::api::apps::v1::StatefulSet;
use k8s::openapi::api::apps::v1::StatefulSetSpec;
use k8s::openapi::api::apps::v1::StatefulSetUpdateStrategy;
use k8s::openapi::api::batch::v1::{Job, JobSpec};
use k8s::openapi::api::core::v1;
use k8s::openapi::api::networking::v1::Ingress;
use k8s::openapi::api::rbac::v1::ClusterRole;
use k8s::openapi::api::rbac::v1::Role;
use k8s::openapi::api::rbac::v1::RoleRef;
use k8s::openapi::api::rbac::v1::Subject;
use k8s::openapi::api::rbac::v1::{ClusterRoleBinding, RoleBinding};
use k8s::openapi::apimachinery::pkg::api::resource::Quantity;
use k8s::openapi::apimachinery::pkg::apis::meta::v1::LabelSelector;
use k8s::openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use k8s::openapi::apimachinery::pkg::util::intstr::IntOrString;
use maplit::btreemap;
use maplit::convert_args;
use statehub_k8s_helper as k8s;

use k8s::ClusterRoleExt;
use k8s::EnvVarExt;
use k8s::NamespaceExt;
use k8s::PolicyRuleExt;
use k8s::RoleBindingExt;
use k8s::RoleExt;
use k8s::ServiceAccountExt;
use k8s::SubjectExt;

use crate::spec::ConfigVolume;
use crate::spec::RuntimeConfig;
use crate::spec::Spec;

use super::*;

const JOB_TTL_SEC: u16 = 60 * 60;

#[must_use]
pub(super) fn as_ingress_spec(config: &RuntimeConfig, spec: &Spec) -> Option<Ingress> {
    const PATH_TYPE: &str = crate::spec::Ingress::PATH_TYPE;
    const ANNOTATION_FN: fn(&crate::spec::IngressSecret) -> (String, String) =
        crate::spec::IngressSecret::annotations;
    use k8s::openapi::api::networking::v1::{
        HTTPIngressPath, HTTPIngressRuleValue, IngressBackend, IngressRule, IngressServiceBackend,
        IngressSpec, IngressTLS, ServiceBackendPort,
    };

    let ingress = spec.service.as_ref()?.ingress()?;
    Some(Ingress {
        metadata: ObjectMeta {
            name: Some(config.name.clone()),
            labels: Some(as_labels(config, spec)),
            annotations: Some(ingress.secret.iter().map(ANNOTATION_FN).collect()),
            ..ObjectMeta::default()
        },
        spec: Some(IngressSpec {
            rules: Some(vec![IngressRule {
                host: Some(config.name.clone()),
                http: Some(HTTPIngressRuleValue {
                    paths: vec![HTTPIngressPath {
                        backend: IngressBackend {
                            service: Some(IngressServiceBackend {
                                name: config.name.clone(),
                                port: Some(ServiceBackendPort {
                                    number: Some(ingress.container_port.port().into()),
                                    name: None,
                                }),
                            }),
                            resource: None,
                        },
                        path: Some(path_to_str(&ingress.path)),
                        path_type: PATH_TYPE.into(),
                    }],
                }),
            }]),
            tls: ingress.secret.as_ref().map(|secret| {
                vec![IngressTLS {
                    hosts: Some(vec![config.name.clone()]),
                    secret_name: Some(secret.name.clone()),
                }]
            }),
            ingress_class_name: Some(ingress.ingress_class.clone()),
            default_backend: None,
        }),
        status: None,
    })
}
#[must_use]
pub(super) fn as_service_spec(
    config: &RuntimeConfig,
    spec: &Spec,
) -> Option<(v1::Service, Option<Ingress>)> {
    let service = spec.service.as_ref()?;

    let port_mapper = |container_port: crate::spec::ContainerPort| v1::ServicePort {
        name: Some(container_port.name()),
        port: container_port.port().into(),
        target_port: Some(convert_port(container_port.port())),
        protocol: Some(container_port.protocol().into()),
        ..v1::ServicePort::default()
    };
    let container_ports = spec.ports.clone().into_iter().map(port_mapper);
    let service_port = service
        .container_port()
        .map(port_mapper)
        .map(|service_port| v1::ServicePort {
            node_port: service.node_port().map(Into::into),
            ..service_port
        });

    let publish = service.publish_not_ready_addresses();
    // This must be set in addition to publishNotReadyAddresses due
    // to an open issue where it may not work:
    // https://github.com/kubernetes/kubernetes/issues/58662
    let annotations = publish.then(|| {
        convert_args!(btreemap!("service.alpha.kubernetes.io/tolerate-unready-endpoints" => "true"))
    });
    let ports = container_ports
        .chain(service_port)
        .collect::<Vec<_>>()
        .with_option();

    let service = v1::Service {
        spec: Some(v1::ServiceSpec {
            type_: Some(service.service_type().into()),
            ports,
            selector: Some(as_selector(config)),
            // We want the servers to become available even if they're not ready
            // since this DNS is also used for join operations.
            publish_not_ready_addresses: Some(publish),
            ..v1::ServiceSpec::default()
        }),
        metadata: ObjectMeta {
            name: Some(config.name.clone()),
            labels: Some(as_labels(config, spec)),
            annotations,
            ..ObjectMeta::default()
        },
        status: None,
    };

    Some((service, as_ingress_spec(config, spec)))
}

pub(super) fn service_account(namespace: impl AsRef<str>) -> String {
    const SERVICE_ACCOUNT: &str = "statehub-service-account";
    format!("{}-{}", SERVICE_ACCOUNT, namespace.as_ref())
}

fn as_selector(config: &RuntimeConfig) -> BTreeMap<String, String> {
    convert_args!(btreemap!(Spec::SELECTOR_NAME_TAG => &config.name))
}

fn as_labels(config: &RuntimeConfig, spec: &Spec) -> BTreeMap<String, String> {
    spec.labels
        .clone()
        .into_iter()
        .chain(as_selector(config))
        .chain(convert_args!(btreemap!("creator" => "statehub-kenie")))
        .collect()
}

#[derive(Debug)]
pub(super) enum EitherDeployment {
    Deployment(Deployment),
    DaemonSet(DaemonSet),
    Job(Job),
    StatefulSet(StatefulSet),
}

impl EitherDeployment {
    /// # Errors
    ///
    /// fail to Serialize `DaemonSet` or `Deployment`
    pub(super) fn as_yaml(&self, writer: impl std::io::Write) -> anyhow::Result<()> {
        match self {
            Self::DaemonSet(d) => to_writer(writer, d),
            Self::Deployment(d) => to_writer(writer, d),
            Self::StatefulSet(s) => to_writer(writer, s),
            Self::Job(job) => to_writer(writer, job),
        }
        .with_context(|| format!("serde {:?}", self))
    }
}

impl From<Deployment> for EitherDeployment {
    fn from(deployment: Deployment) -> Self {
        Self::Deployment(deployment)
    }
}

impl From<StatefulSet> for EitherDeployment {
    fn from(statefulset: StatefulSet) -> Self {
        Self::StatefulSet(statefulset)
    }
}

impl From<DaemonSet> for EitherDeployment {
    fn from(daemon_set: DaemonSet) -> Self {
        Self::DaemonSet(daemon_set)
    }
}

impl From<Job> for EitherDeployment {
    fn from(job: Job) -> Self {
        Self::Job(job)
    }
}

pub(super) fn as_either_deployment_specs(
    account: impl AsRef<str>,
    config: &RuntimeConfig,
    spec: &Spec,
) -> anyhow::Result<EitherDeployment> {
    use crate::spec::DeploymentStrategy::{OnDelete, Recreate};
    Ok(match &spec.deployment {
        DeploymentKind::ReplicaSet { strategy, .. } if *strategy != OnDelete => {
            as_deployment_spec(account, config, spec, *strategy).into()
        }
        DeploymentKind::StatefulSet { strategy, .. } if *strategy != OnDelete => {
            as_statefulset_spec(account, config, spec, *strategy).into()
        }
        DeploymentKind::DaemonSet { strategy, .. } if *strategy != Recreate => {
            as_daemon_set_spec(account, config, spec, *strategy).into()
        }
        DeploymentKind::Job { backoff_limit, .. } => {
            as_job_spec(account, config, spec, *backoff_limit).into()
        }
        _ => anyhow::bail!("invalid deployment strategy for {:?}", spec.deployment),
    })
}

#[must_use]
fn as_daemon_set_spec(
    service_account_name: impl AsRef<str>,
    config: &RuntimeConfig,
    spec: &Spec,
    strategy: crate::spec::DeploymentStrategy,
) -> DaemonSet {
    DaemonSet {
        metadata: container_md(config, spec),
        spec: Some(DaemonSetSpec {
            // Can be "RollingUpdate" or "OnDelete". Default is RollingUpdate
            update_strategy: Some(DaemonSetUpdateStrategy {
                type_: Some(strategy.as_str().into()),
                rolling_update: None,
            }),
            selector: LabelSelector {
                match_labels: Some(as_labels(config, spec)),
                match_expressions: None,
            },
            template: v1::PodTemplateSpec {
                metadata: Some(ObjectMeta {
                    labels: Some(as_labels(config, spec)),
                    ..ObjectMeta::default()
                }),
                spec: Some(as_pod_spec(service_account_name, config, spec)),
            },
            ..DaemonSetSpec::default()
        }),
        status: None,
    }
}

#[must_use]
fn as_job_spec(
    service_account_name: impl AsRef<str>,
    config: &RuntimeConfig,
    spec: &Spec,
    backoff_limit: u16,
) -> Job {
    // jobs use `generate_name` instead of `name` so they can be re-submitted as needed
    let metadata = ObjectMeta {
        generate_name: Some(config.name.clone()),
        labels: Some(as_labels(config, spec)),
        ..ObjectMeta::default()
    };
    let ttl_seconds_after_finished = JOB_TTL_SEC.into();

    Job {
        metadata,
        spec: Some(JobSpec {
            ttl_seconds_after_finished: Some(ttl_seconds_after_finished),
            backoff_limit: Some(backoff_limit.into()),
            template: v1::PodTemplateSpec {
                metadata: Some(ObjectMeta {
                    labels: Some(as_labels(config, spec)),
                    ..ObjectMeta::default()
                }),
                spec: Some(as_pod_spec(service_account_name, config, spec)),
            },
            ..JobSpec::default()
        }),
        status: None,
    }
}

#[must_use]
fn as_deployment_spec(
    service_account_name: impl AsRef<str>,
    config: &RuntimeConfig,
    spec: &Spec,
    strategy: crate::spec::DeploymentStrategy,
) -> Deployment {
    Deployment {
        metadata: container_md(config, spec),
        spec: Some(DeploymentSpec {
            strategy: Some(DeploymentStrategy {
                type_: Some(strategy.as_str().into()),
                rolling_update: None,
            }),
            replicas: spec.deployment.replicas().map(Into::into),
            selector: LabelSelector {
                match_labels: Some(as_labels(config, spec)),
                match_expressions: None,
            },
            template: v1::PodTemplateSpec {
                metadata: Some(ObjectMeta {
                    labels: Some(as_labels(config, spec)),
                    ..ObjectMeta::default()
                }),
                spec: Some(as_pod_spec(service_account_name, config, spec)),
            },
            ..DeploymentSpec::default()
        }),
        status: None,
    }
}

#[must_use]
fn as_statefulset_spec(
    service_account_name: impl AsRef<str>,
    config: &RuntimeConfig,
    spec: &Spec,
    strategy: crate::spec::DeploymentStrategy,
) -> StatefulSet {
    StatefulSet {
        metadata: container_md(config, spec),
        spec: Some(StatefulSetSpec {
            replicas: spec.deployment.replicas().map(Into::into),
            selector: LabelSelector {
                match_labels: Some(as_labels(config, spec)),
                match_expressions: None,
            },
            template: v1::PodTemplateSpec {
                metadata: Some(ObjectMeta {
                    labels: Some(as_labels(config, spec)),
                    ..ObjectMeta::default()
                }),
                spec: Some(as_pod_spec(service_account_name, config, spec)),
            },
            update_strategy: Some(StatefulSetUpdateStrategy {
                type_: Some(strategy.as_str().into()),
                rolling_update: None,
            }),
            ..StatefulSetSpec::default()
        }),
        status: None,
    }
}
pub(super) fn volume_as_config_map(volume: &ConfigVolume) -> v1::ConfigMap {
    let data = volume
        .files
        .iter()
        .filter_map(|(path, content)| file_name(path).zip(Some(content.clone())))
        .collect::<BTreeMap<_, _>>()
        .with_option();

    v1::ConfigMap {
        metadata: object_md(&volume.mount.name),
        data,
        ..v1::ConfigMap::default()
    }
}

pub(super) fn as_config_map(spec: &Spec) -> impl Iterator<Item = v1::ConfigMap> + '_ {
    spec.config_volumes.iter().map(volume_as_config_map)
}

pub(super) fn as_secret(config: &RuntimeConfig, spec: &Spec) -> Vec<v1::Secret> {
    spec.secrets
        .iter()
        .filter_map(|(name, value)| secret(name, value.as_ref()?, as_labels(config, spec)))
        .collect()
}

pub fn secret(
    name: &str,
    secret: &HashMap<String, String>,
    labels: BTreeMap<String, String>,
) -> Option<v1::Secret> {
    secret
        .iter()
        .map(|(key, value)| (key.clone(), value.clone()))
        .collect::<BTreeMap<_, _>>()
        .with_option()
        .map(|string_data| {
            let metadata = ObjectMeta {
                name: Some(name.into()),
                labels: labels.with_option(),
                ..ObjectMeta::default()
            };

            v1::Secret {
                metadata,
                string_data: string_data.with_option(),
                // https://kubernetes.io/docs/concepts/configuration/secret/#opaque-secrets
                type_: Some("Opaque".into()),
                ..v1::Secret::default()
            }
        })
}

fn convert_port(port: u16) -> IntOrString {
    IntOrString::Int(port.into())
}
fn path_to_str(path: &std::path::Path) -> String {
    path.to_string_lossy().into_owned()
}

fn file_name(path: &std::path::Path) -> Option<String> {
    Some(path.file_name()?.to_string_lossy().into_owned())
}

/// `common_env_vars` extended with node-name -
/// so we can know what is our actual `node` in case we are part of a `daemon-set`
fn common_env_vars(config: &RuntimeConfig, spec: &Spec) -> Vec<corev1::EnvVar> {
    spec.merge_env(config)
        .into_iter()
        .map(|(key, value)| corev1::EnvVar::value(key, value))
        .chain(crate::env::all())
        .unique_by(|env_var| env_var.name.clone())
        .collect()
}

fn container_port(spec: &Spec) -> Option<Vec<v1::ContainerPort>> {
    let service_port = spec
        .service
        .as_ref()
        .and_then(crate::spec::Service::container_port);
    spec.ports
        .iter()
        .chain(&service_port)
        .map(|&port| v1::ContainerPort {
            container_port: port.port().into(),
            protocol: Some(port.protocol().into()),
            ..v1::ContainerPort::default()
        })
        .collect::<Vec<_>>()
        .with_option()
}

/// convert `rexconf::spec::Probe` into k8s probe definition
fn probe(probe: &crate::spec::Probe) -> v1::Probe {
    use crate::spec::ProbeThresholds::{
        self, FailureThreshold, InitialDelaySec, PeriodSec, SuccessThreshold, TimeoutSec,
    };
    use v1::{ExecAction, HTTPGetAction, TCPSocketAction};

    let threshold = |typ: ProbeThresholds| Some(i32::from(*probe.thresholds.get(&typ)?));

    v1::Probe {
        tcp_socket: probe.method.as_tcp_socket().map(|&port| TCPSocketAction {
            port: convert_port(port),
            ..TCPSocketAction::default()
        }),
        http_get: probe.method.as_http().map(|(port, path)| HTTPGetAction {
            port: convert_port(port),
            path: Some(path_to_str(path)),
            ..HTTPGetAction::default()
        }),
        exec: probe
            .method
            .exec()
            .map(|cmd| ExecAction { command: Some(cmd) }),
        success_threshold: threshold(SuccessThreshold),
        failure_threshold: threshold(FailureThreshold),
        initial_delay_seconds: threshold(InitialDelaySec),
        period_seconds: threshold(PeriodSec),
        timeout_seconds: threshold(TimeoutSec),
        ..v1::Probe::default()
    }
}

fn volumes(container: &Spec) -> (Option<Vec<v1::Volume>>, Option<Vec<v1::VolumeMount>>) {
    use crate::spec::{ContainerVolumeMount, HugePages};
    // define how the volumes are exposed to the container
    let volumes = container
        .volumes
        .iter()
        .map(|volume| (&volume.mount, Some(volume.path_type.read_only())));
    let config_volumes = container
        .config_volumes
        .iter()
        .map(|volume| (&volume.mount, None));

    let hugepages = container
        .resource_limits
        .hub_pages
        .map(|_| ContainerVolumeMount {
            name: HugePages::VOLUME_NAME.into(),
            path: HugePages::MOUNT_PATH.into(),
        });
    // hugepages need to be written to; read_only = false
    let hugepages = hugepages.as_ref().zip(Some(Some(false)));

    let mounts = config_volumes
        .chain(volumes)
        .chain(hugepages)
        .map(|(mount, read_only)| v1::VolumeMount {
            name: mount.name.clone(),
            mount_path: path_to_str(&mount.path),
            read_only,
            ..v1::VolumeMount::default()
        })
        .collect::<Vec<_>>()
        .with_option();

    let hosts = container.volumes.iter().map(|volume| v1::Volume {
        name: volume.mount.name.clone(),
        host_path: Some(v1::HostPathVolumeSource {
            path: path_to_str(&volume.host_path),
            type_: Some(volume.path_type.as_str().into()),
        }),
        ..v1::Volume::default()
    });
    // hugepages need an empty-dir of medium HugePages
    let huge_pages_source = container.resource_limits.hub_pages.map(|_| v1::Volume {
        name: HugePages::VOLUME_NAME.into(),
        empty_dir: Some(v1::EmptyDirVolumeSource {
            medium: Some(HugePages::MEDIUM.into()),
            size_limit: None,
        }),
        ..v1::Volume::default()
    });

    let config_maps = container.config_volumes.iter().map(|volume| v1::Volume {
        name: volume.mount.name.clone(),
        config_map: Some(v1::ConfigMapVolumeSource {
            name: Some(volume.mount.name.clone()),
            items: volume
                .files
                .keys()
                .filter_map(|path| file_name(path).zip(Some(path_to_str(path))))
                .map(|(key, path)| v1::KeyToPath {
                    key,
                    path,
                    mode: None,
                })
                .collect::<Vec<_>>()
                .with_option(),
            ..v1::ConfigMapVolumeSource::default()
        }),
        ..v1::Volume::default()
    });

    let volumes = hosts
        .chain(config_maps)
        .chain(huge_pages_source)
        .collect::<Vec<_>>()
        .with_option();

    (volumes, mounts)
}
fn as_pod_resource_requirements(spec: &Spec) -> Option<v1::ResourceRequirements> {
    use crate::spec::ResourceRequirements;
    let resource_requirements = |resources: ResourceRequirements| {
        resources
            .iter()
            .map(|(key, quantity)| (key, Quantity(quantity)))
            .collect::<BTreeMap<_, _>>()
            .with_option()
    };
    let limits = resource_requirements(spec.resource_limits);
    let requests = resource_requirements(spec.resource_requests);

    (limits.is_some() || requests.is_some()).then(|| v1::ResourceRequirements { limits, requests })
}

fn as_pod_spec(
    service_account_name: impl AsRef<str>,
    config: &RuntimeConfig,
    spec: &Spec,
) -> v1::PodSpec {
    use crate::spec::ProbeGoal::{Liveness, Readiness, Startup};
    let tolerations = spec.deployment.toleration().map(|toleration| {
        vec![v1::Toleration {
            effect: toleration.effect.as_str().map(Into::into),
            toleration_seconds: toleration.effect.secs().map(Into::into),
            operator: Some(toleration.operator.as_str().into()),
            key: toleration.operator.key().map(Into::into),
            value: toleration.operator.value().map(Into::into),
        }]
    });
    let (volumes, volume_mounts) = volumes(spec);
    let secrets_env = spec
        .secrets
        .keys()
        .map(|secret_name| v1::EnvFromSource {
            secret_ref: Some(v1::SecretEnvSource {
                name: Some(secret_name.clone()),
                optional: Some(false),
            }),
            ..v1::EnvFromSource::default()
        })
        .collect();

    v1::PodSpec {
        volumes,
        containers: vec![v1::Container {
            name: config.name.clone(),
            args: spec.args.clone().with_option(),
            command: spec.commands.clone().with_option(),
            env: Some(common_env_vars(config, spec)),
            env_from: Some(secrets_env),
            ports: container_port(spec),
            image: Some(config.image.clone()),
            volume_mounts,
            liveness_probe: spec.probes.get(&Liveness).map(probe),
            readiness_probe: spec.probes.get(&Readiness).map(probe),
            startup_probe: spec.probes.get(&Startup).map(probe),
            security_context: Some(v1::SecurityContext {
                privileged: Some(spec.privileged),
                allow_privilege_escalation: Some(spec.privileged),
                ..v1::SecurityContext::default()
            }),
            resources: as_pod_resource_requirements(spec),
            image_pull_policy: spec.image_pull_policy.map(|policy| policy.as_str().into()),
            ..v1::Container::default()
        }],
        restart_policy: Some(spec.restart_policy.as_str().into()),
        host_network: Some(spec.host_network),
        host_pid: Some(spec.host_pid),
        dns_policy: Some(spec.dns_policy.as_str().into()),
        service_account_name: Some(service_account_name.as_ref().into()),
        node_selector: spec.deployment.node_selector().with_option(),
        tolerations,
        ..v1::PodSpec::default()
    }
}

fn object_md(name: impl AsRef<str>) -> ObjectMeta {
    ObjectMeta {
        name: Some(name.as_ref().into()),
        ..ObjectMeta::default()
    }
}

fn container_md(config: &RuntimeConfig, spec: &Spec) -> ObjectMeta {
    ObjectMeta {
        name: Some(config.name.clone()),
        labels: as_labels(config, spec).with_option(),
        owner_references: config
            .owner_reference
            .clone()
            .into_iter()
            .collect::<Vec<_>>()
            .with_option(),
        ..ObjectMeta::default()
    }
}

#[must_use]
pub(super) fn account(
    role_name: impl AsRef<str>,
    account_name: impl AsRef<str>,
    image_pull_secret: impl AsRef<str>,
    namespace: impl AsRef<str>,
) -> (v1::ServiceAccount, Role, RoleBinding) {
    let service_account = mgmt_service_account(image_pull_secret, &account_name);
    let role = mgmt_role(role_name);
    let role_binding = mgmt_role_binding(&role, account_name, namespace);
    (service_account, role, role_binding)
}

#[must_use]
pub(super) fn cluster_account(
    cluster_role_name: impl AsRef<str>,
    account_name: impl AsRef<str>,
    image_pull_secret: impl AsRef<str>,
    namespace: impl AsRef<str>,
) -> (
    v1::Namespace,
    v1::ServiceAccount,
    ClusterRole,
    ClusterRoleBinding,
) {
    (
        mgmt_namespace(&namespace),
        mgmt_service_account(image_pull_secret, &account_name),
        mgmt_cluster_role(&cluster_role_name),
        mgmt_cluster_role_binding(cluster_role_name, account_name, &namespace),
    )
}

pub(super) fn mgmt_namespace(namespace: impl AsRef<str>) -> corev1::Namespace {
    corev1::Namespace::new(namespace.as_ref())
}

fn mgmt_service_account(
    image_pull_secret: impl AsRef<str>,
    account_name: impl AsRef<str>,
) -> corev1::ServiceAccount {
    corev1::ServiceAccount::new(account_name.as_ref()).image_pull_secret(image_pull_secret.as_ref())
}

fn mgmt_cluster_role(cluster_role_name: impl AsRef<str>) -> ClusterRole {
    use kube::discovery::verbs::{
        CREATE, DELETE, DELETE_COLLECTION, GET, LIST, PATCH, UPDATE, WATCH,
    };

    let verbs = &[
        CREATE,
        DELETE,
        DELETE_COLLECTION,
        GET,
        LIST,
        PATCH,
        UPDATE,
        WATCH,
    ];

    let rules = [
        rbacv1::PolicyRule::new::<corev1::ConfigMap>().verbs(verbs),
        rbacv1::PolicyRule::new::<corev1::Secret>().verbs(verbs),
        rbacv1::PolicyRule::new::<corev1::Pod>()
            .with_status()
            .verbs(verbs),
        rbacv1::PolicyRule::new::<appsv1::DaemonSet>()
            .with_status()
            .verbs(verbs),
        rbacv1::PolicyRule::new::<appsv1::Deployment>()
            .with_status()
            .verbs(verbs),
        rbacv1::PolicyRule::new::<appsv1::StatefulSet>()
            .with_status()
            .verbs(verbs),
        rbacv1::PolicyRule::new::<batchv1::Job>()
            .with_status()
            .verbs(verbs),
        rbacv1::PolicyRule::new::<corev1::Service>()
            .with_status()
            .verbs(verbs),
    ];
    rbacv1::ClusterRole::new(cluster_role_name.as_ref()).rules(rules)
}

fn mgmt_role(cluster_role_name: impl AsRef<str>) -> rbacv1::Role {
    use kube::discovery::verbs::{CREATE, GET, LIST, PATCH, UPDATE, WATCH};

    let rules = [
        rbacv1::PolicyRule::new::<corev1::ConfigMap>()
            .verbs([CREATE, GET, LIST, PATCH, UPDATE, WATCH]),
        rbacv1::PolicyRule::new::<corev1::Pod>().verbs([GET, LIST, WATCH]),
    ];
    rbacv1::Role::new(cluster_role_name.as_ref()).rules(rules)
}

pub(super) fn mgmt_role_binding(
    role: &rbacv1::Role,
    account_name: impl AsRef<str>,
    namespace: impl AsRef<str>,
) -> rbacv1::RoleBinding {
    let account_name = account_name.as_ref();
    let namespace = namespace.as_ref();
    let name = format!("{}-{}-{}", account_name, namespace, role.name());
    let subjects = [rbacv1::Subject::service_account(account_name).namespace(namespace)];
    rbacv1::RoleBinding::new(name, role).subjects(subjects)
}

pub(super) fn mgmt_cluster_role_binding(
    role_name: impl AsRef<str>,
    account_name: impl AsRef<str>,
    namespace: impl AsRef<str>,
) -> ClusterRoleBinding {
    ClusterRoleBinding {
        metadata: object_md(format!(
            "{}-{}-{}",
            account_name.as_ref(),
            namespace.as_ref(),
            role_name.as_ref()
        )),
        role_ref: RoleRef {
            kind: ClusterRole::KIND.into(),
            name: role_name.as_ref().into(),
            api_group: API_GROUPS.into(),
        },
        subjects: Some(vec![Subject {
            api_group: None,
            kind: v1::ServiceAccount::KIND.into(),
            name: account_name.as_ref().into(),
            namespace: Some(namespace.as_ref().into()),
        }]),
    }
}
