//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//
use std::collections::{BTreeMap, HashMap};
use std::fmt;
use std::ops::Range;
use std::path::PathBuf;

use num_integer::Integer;
use serde::{Deserialize, Serialize};
use statehub_k8s_helper::metav1::OwnerReference;

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct Spec {
    pub environment: Environment,
    pub volumes: Vec<DataVolume>,
    pub config_volumes: Vec<ConfigVolume>,
    pub commands: Vec<String>,
    pub args: Vec<String>,
    pub ports: Vec<ContainerPort>,
    pub labels: HashMap<String, String>,
    pub deployment: DeploymentKind,
    pub service: Option<Service>,
    pub host_network: bool,
    pub privileged: bool,
    pub host_pid: bool,
    pub probes: HashMap<ProbeGoal, Probe>,
    pub resource_limits: ResourceRequirements,
    pub resource_requests: ResourceRequirements,
    pub restart_policy: RestartPolicy,
    pub image_pull_policy: Option<ImagePullPolicy>,
    pub dns_policy: DNSPolicy,
    ///secret name to environment variable name and its value
    ///if the value is none, the secret should be already applied
    pub secrets: HashMap<String, Option<HashMap<String, String>>>,
}

impl Spec {
    pub const SELECTOR_NAME_TAG: &'static str = "app";

    #[cfg(feature = "kubectl")]
    pub(crate) fn merge_env(&self, config: &RuntimeConfig) -> Environment {
        let mut environment = self.environment.clone();
        environment.extend(config.environment.clone());
        environment
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RuntimeConfig {
    /// extension of the setup-steps defined in container[profile].setup
    pub environment: Environment,

    pub image: String,

    pub name: String,

    pub owner_reference: Option<OwnerReference>,
}

pub trait GetGetName {
    fn get_name(&self) -> &str;
}

impl GetGetName for RuntimeConfig {
    fn get_name(&self) -> &str {
        &self.name
    }
}

pub trait AsWorkloadRef {
    fn as_workload_ref(&self) -> WorkloadRef<'_>;
}

/// a helper type used for reploying a workload-stack
#[derive(Copy, Clone, Debug)]
pub struct WorkloadRef<'a> {
    pub config: &'a RuntimeConfig,
    pub spec: &'a Spec,
}

impl<'a> AsWorkloadRef for WorkloadRef<'a> {
    fn as_workload_ref(&self) -> Self {
        *self
    }
}

/// a helper type used for deploying a workload-stack
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Workload {
    pub config: RuntimeConfig,
    pub spec: Spec,
}

impl<'a> GetGetName for WorkloadRef<'a> {
    fn get_name(&self) -> &str {
        self.config.get_name()
    }
}

impl AsWorkloadRef for Workload {
    fn as_workload_ref(&self) -> WorkloadRef<'_> {
        WorkloadRef {
            config: &self.config,
            spec: &self.spec,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct WorkloadStack<T> {
    /// namespace to run the workload at
    pub namespace: Option<String>,
    /// active workloads - ordered the key ot the tress
    pub active: BTreeMap<usize, Vec<T>>,
    /// workloads that where removed from `Self::workloads` -
    /// typically when a volume is removed
    pub graveyard: Vec<T>,
}

impl<T> Default for WorkloadStack<T> {
    fn default() -> Self {
        Self {
            namespace: None,
            active: BTreeMap::new(),
            graveyard: vec![],
        }
    }
}

impl<T: GetGetName> WorkloadStack<T> {
    const GRAVEYARD_DEPTH: usize = 50;

    #[cfg(feature = "kubectl")]
    pub(crate) fn sanitize(&self) -> anyhow::Result<()>
    where
        T: fmt::Debug,
    {
        use itertools::Itertools;

        let workload_to_name = self
            .active
            .values()
            .flatten()
            .map(|workload| (workload.get_name(), workload))
            .into_group_map();
        for (name, workloads) in workload_to_name {
            anyhow::ensure!(
                !name.is_empty(),
                "empty workload names are not allowed {:?}",
                workloads
            );
            anyhow::ensure!(
                workloads.len() <= 1,
                "duplicated workload names are not allowed {:?}",
                workloads
            );
        }
        Ok(())
    }

    pub fn graveyard(&mut self, name: &str) {
        for workloads in self.active.values_mut() {
            for workload in std::mem::take(workloads) {
                if workload.get_name() == name {
                    let contains = self
                        .graveyard
                        .iter()
                        .any(|workload| workload.get_name() == name);
                    if !contains {
                        self.graveyard.push(workload)
                    }
                } else {
                    workloads.push(workload)
                }
            }
        }
        self.active.retain(|_, workloads| !workloads.is_empty());

        // retain the last `Self::GRAVEYARD_DEPTH` containers
        let len = self.graveyard.len();
        if let Some(at) = len.checked_sub(Self::GRAVEYARD_DEPTH) {
            self.graveyard = self.graveyard.split_off(at)
        }
    }

    pub fn extend_active(&mut self, active: &BTreeMap<usize, Vec<T>>)
    where
        T: Clone,
    {
        for (&order, workloads) in active {
            let entry = self.active.entry(order).or_default();
            entry.extend_from_slice(workloads);
            *entry = std::mem::take(entry)
                .into_iter()
                .map(|config| (config.get_name().to_string(), config))
                .collect::<HashMap<_, _>>()
                .into_values()
                .collect();
        }
    }
}

/// how are we probing?dex
#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum ProbeMethod {
    TcpSocket { port: u16 },
    HttpGet { port: u16, path: PathBuf },
    Exec { command: Vec<String> },
}

impl ProbeMethod {
    pub fn tcp_socket(port: u16) -> Self {
        Self::TcpSocket { port }
    }

    pub fn as_tcp_socket(&self) -> Option<&u16> {
        if let Self::TcpSocket { ref port } = self {
            Some(port)
        } else {
            None
        }
    }

    pub fn as_http(&self) -> Option<(u16, &std::path::Path)> {
        if let Self::HttpGet { port, path } = self {
            Some((*port, path))
        } else {
            None
        }
    }

    pub fn exec(&self) -> Option<Vec<String>> {
        if let Self::Exec { command } = self {
            Some(command.clone())
        } else {
            None
        }
    }
}

/// what are we probing for?
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub enum ProbeGoal {
    Readiness,
    /// does not wait for `readiness`
    Liveness,
    /// like `liveness` waits for `readiness`
    Startup,
}

#[derive(Copy, Clone, Debug, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub enum ProbeThresholds {
    InitialDelaySec,
    PeriodSec,
    TimeoutSec,
    SuccessThreshold,
    FailureThreshold,
}
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Probe {
    pub method: ProbeMethod,
    pub thresholds: HashMap<ProbeThresholds, u16>,
}

#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub struct HugePages {
    pub chunk_size_mb: usize,
    pub quantity_mb: usize,
}
impl HugePages {
    pub const MEDIUM: &'static str = "HugePages";
    pub const MOUNT_PATH: &'static str = "/hugepages";
    pub const VOLUME_NAME: &'static str = "hugepage";
}

fn suffixed(value_mb: usize) -> String {
    static SUFFIXES: [(Range<usize>, &str); 5] = [
        (1 << 20..1 << 30, "Mi"),
        (1 << 30..1 << 40, "Gi"),
        (1 << 40..1 << 50, "Ti"),
        (1 << 50..1 << 60, "Pi"),
        (1 << 60..usize::MAX, "Ei"),
    ];
    let non_zero = |value: usize| if value == 0 { 1 } else { value };
    let value = non_zero(value_mb) * 1024 * 1024;

    let (range, suffix) = SUFFIXES
        .iter()
        .find(|(range, _)| range.contains(&value))
        .unwrap_or(&SUFFIXES[SUFFIXES.len() - 1]);

    let normalized = non_zero(Integer::div_ceil(&value, &range.start));

    format!("{}{}", normalized, suffix)
}

impl HugePages {
    fn format(self) -> (String, String) {
        (
            format!("hugepages-{}", suffixed(self.chunk_size_mb)),
            suffixed(self.quantity_mb),
        )
    }
}

#[derive(Copy, Clone, Debug, Default, Deserialize, Serialize)]
pub struct ResourceRequirements {
    /// use we `millicpu` i.e. `250m` over `0.25`
    pub cpu: Option<usize>,
    pub memory_mb: Option<usize>,
    /// huge-pages, allocated in units of power-of-2
    /// if you want more than one kinds of huge-pages - too bad
    pub hub_pages: Option<HugePages>,
}

impl ResourceRequirements {
    pub fn iter(self) -> impl Iterator<Item = (String, String)> {
        self.cpu
            .into_iter()
            .map(|cpu| ("cpu".into(), format!("{}m", cpu)))
            .chain(
                self.memory_mb
                    .into_iter()
                    .map(move |memory| ("memory".into(), suffixed(memory))),
            )
            .chain(self.hub_pages.into_iter().map(move |hp| hp.format()))
    }
}

#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub enum RestartPolicy {
    Always,
    OnFailure,
    Never,
}

impl RestartPolicy {
    pub const fn as_str(self) -> &'static str {
        match self {
            Self::Always => "Always",
            Self::OnFailure => "OnFailure",
            Self::Never => "Never",
        }
    }
}
impl Default for RestartPolicy {
    fn default() -> Self {
        Self::Always
    }
}

#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub enum ImagePullPolicy {
    Always,
    IfNotPresent,
    Never,
}

impl ImagePullPolicy {
    pub const fn as_str(self) -> &'static str {
        match self {
            Self::Always => "Always",
            Self::IfNotPresent => "IfNotPresent",
            Self::Never => "Never",
        }
    }
}

/// k8s host-types
#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub enum HostPathType {
    /// Empty string (default) is for backward compatibility, which means that no checks will be performed before mounting the hostPath volume.
    None,
    /// If nothing exists at the given path, an empty directory will be created there as needed with permission set to 0755, having the same group and ownership with Kubelet.
    DirectoryOrCreate,
    /// A directory must exist at the given path
    Directory,
    /// If nothing exists at the given path, an empty file will be created there as needed with permission set to 0644, having the same group and ownership with Kubelet.
    FileOrCreate,
    /// A file must exist at the given path
    File,
    /// A UNIX socket must exist at the given path
    Socket,
    /// A character device must exist at the given path
    CharDevice,
    /// A block device must exist at the given path
    BlockDevice,
}
impl Default for HostPathType {
    fn default() -> Self {
        Self::DirectoryOrCreate
    }
}

impl HostPathType {
    pub fn read_only(self) -> bool {
        match self {
            Self::Directory | Self::File => true,
            Self::DirectoryOrCreate
            | Self::FileOrCreate
            | Self::Socket
            | Self::CharDevice
            | Self::BlockDevice
            | Self::None => false,
        }
    }
    pub fn as_str(self) -> &'static str {
        match self {
            Self::None => "None",
            Self::DirectoryOrCreate => "DirectoryOrCreate",
            Self::Directory => "Directory",
            Self::FileOrCreate => "FileOrCreate",
            Self::File => "File",
            Self::Socket => "Socket",
            Self::CharDevice => "CharDevice",
            Self::BlockDevice => "BlockDevice",
        }
    }
}

/// Set DNS policy for the pod.
/// Defaults to "ClusterFirst".
/// Valid values are
/// 'ClusterFirstWithHostNet',
/// 'ClusterFirst',
/// 'Default'
/// or 'None'.
/// DNS parameters given in DNSConfig will be merged with the policy selected with DNSPolicy.
/// To have DNS options set along with hostNetwork,
/// you have to specify DNS policy explicitly to 'ClusterFirstWithHostNet'.
#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub enum DNSPolicy {
    ClusterFirst,
    ClusterFirstWithHostNet,
    Default,
}

impl Default for DNSPolicy {
    fn default() -> Self {
        // set default-DNS-policy to be `ClusterFirstWithHostNet`
        // instead of `ClusterFirst` since it better feats our need
        Self::ClusterFirstWithHostNet
    }
}

impl DNSPolicy {
    pub fn as_str(self) -> &'static str {
        match self {
            Self::ClusterFirst => "ClusterFirst",
            Self::ClusterFirstWithHostNet => "ClusterFirstWithHostNet",
            Self::Default => "Default",
        }
    }
}

#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub enum ContainerPort {
    Udp(u16),
    Tcp(u16),
}

impl Default for ContainerPort {
    fn default() -> Self {
        Self::Tcp(0)
    }
}

impl From<u16> for ContainerPort {
    fn from(port: u16) -> Self {
        Self::Tcp(port)
    }
}

impl ContainerPort {
    pub fn name(&self) -> String {
        format!("{}-{}", self.protocol().to_ascii_lowercase(), self.port())
    }

    pub fn protocol(self) -> &'static str {
        match self {
            Self::Udp(_) => "UDP",
            Self::Tcp(_) => "TCP",
        }
    }

    pub fn port(self) -> u16 {
        match self {
            Self::Tcp(port) | Self::Udp(port) => port,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct IngressSecret {
    pub issuer: String,
    pub name: String,
}

impl IngressSecret {
    const ISSUER_KEY: &'static str = "cert-manager.io/cluster-issuer";
    pub fn annotations(&self) -> (String, String) {
        (Self::ISSUER_KEY.into(), self.issuer.clone())
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Ingress {
    pub container_port: ContainerPort,
    pub path: PathBuf,
    pub ingress_class: String,
    pub secret: Option<IngressSecret>,
}

impl Ingress {
    pub const PATH_TYPE: &'static str = "Prefix";
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Service {
    ClusterIp {
        publish_not_ready_addresses: bool,
    },
    NodePort {
        port: u16,
        container_port: ContainerPort,
        publish_not_ready_addresses: bool,
    },
    Ingress {
        ingress: Ingress,
        publish_not_ready_addresses: bool,
    },
}

impl Default for Service {
    fn default() -> Self {
        Self::ClusterIp {
            publish_not_ready_addresses: false,
        }
    }
}
impl Service {
    pub fn ingress(&self) -> Option<&Ingress> {
        if let Self::Ingress { ingress, .. } = self {
            Some(ingress)
        } else {
            None
        }
    }

    pub fn default_cluster_ip() -> Self {
        Self::default()
    }

    pub fn service_type(&self) -> &'static str {
        match self {
            Self::ClusterIp { .. } | Self::Ingress { .. } => "ClusterIP",
            Self::NodePort { .. } => "NodePort",
        }
    }

    pub fn publish_not_ready_addresses(&self) -> bool {
        let (Self::ClusterIp {
            publish_not_ready_addresses,
            ..
        }
        | Self::NodePort {
            publish_not_ready_addresses,
            ..
        }
        | Self::Ingress {
            publish_not_ready_addresses,
            ..
        }) = self;
        *publish_not_ready_addresses
    }

    pub fn container_port(&self) -> Option<ContainerPort> {
        if let Self::NodePort { container_port, .. }
        | Self::Ingress {
            ingress: Ingress { container_port, .. },
            ..
        } = self
        {
            Some(*container_port)
        } else {
            None
        }
    }
    pub fn node_port(&self) -> Option<u16> {
        if let Self::NodePort { port, .. } = self {
            Some(*port)
        } else {
            None
        }
    }
}
#[derive(Copy, Clone, Debug, PartialEq, Deserialize, Serialize)]
pub enum DeploymentStrategy {
    /// default-value - relevant for both `deployment` and `daemon-set`
    RollingUpdate,
    /// relevant for `deployment` only
    Recreate,
    /// relevant for `daemon-set` only
    OnDelete,
}
impl DeploymentStrategy {
    pub(crate) fn as_str(self) -> &'static str {
        match self {
            Self::RollingUpdate => "RollingUpdate",
            Self::Recreate => "Recreate",
            Self::OnDelete => "OnDelete",
        }
    }
}

impl fmt::Display for DeploymentStrategy {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.as_str().fmt(f)
    }
}

impl Default for DeploymentStrategy {
    fn default() -> Self {
        Self::RollingUpdate
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum DeploymentKind {
    ReplicaSet {
        replicas: u16,
        strategy: DeploymentStrategy,
        node_selector: HashMap<String, String>,
    },
    StatefulSet {
        replicas: u16,
        strategy: DeploymentStrategy,
        node_selector: HashMap<String, String>,
    },
    DaemonSet {
        with_tolerations: bool,
        strategy: DeploymentStrategy,
    },
    Job {
        backoff_limit: u16,
        node_selector: HashMap<String, String>,
    },
}
impl Default for DeploymentKind {
    fn default() -> Self {
        Self::ReplicaSet {
            replicas: 1,
            strategy: DeploymentStrategy::RollingUpdate,
            node_selector: HashMap::new(),
        }
    }
}

impl DeploymentKind {
    pub fn node_selector(&self) -> BTreeMap<String, String> {
        match self {
            Self::StatefulSet { node_selector, .. }
            | Self::ReplicaSet { node_selector, .. }
            | Self::Job { node_selector, .. } => node_selector.clone().into_iter().collect(),
            _ => BTreeMap::new(),
        }
    }

    pub fn backoff_limit(&self) -> Option<u16> {
        if let Self::Job { backoff_limit, .. } = self {
            Some(*backoff_limit)
        } else {
            None
        }
    }
    /// https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/
    /// tolerations:
    ///       # this toleration is to have the daemonset runnable on master nodes
    ///       # remove it if your masters can't run pods
    ///       - key: node-role.kubernetes.io/master
    ///         operator: Exists
    ///         effect: NoSchedule
    pub fn toleration(&self) -> Option<Toleration> {
        matches!(
            self,
            Self::DaemonSet {
                with_tolerations: true,
                ..
            }
        )
        .then(|| Toleration {
            operator: TolerationOperator::Exists {
                key: Some("node-role.kubernetes.io/master".into()),
            },
            effect: TolerationEffect::NoSchedule,
        })
    }

    pub fn replicas(&self) -> Option<u16> {
        match self {
            Self::ReplicaSet { replicas, .. } | Self::StatefulSet { replicas, .. } => {
                Some(*replicas)
            }
            _ => None,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Toleration {
    pub operator: TolerationOperator,
    pub effect: TolerationEffect,
}

/// Operator represents a key's relationship to the value.
/// Valid operators are Exists and Equal.
/// Defaults to Equal.
/// Exists is equivalent to wildcard for value, so that a pod can tolerate all taints of a particular category.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum TolerationOperator {
    /// Exists is equivalent to wildcard for value, so that a pod can tolerate all taints of a particular category.
    /// If the key is empty, operator must be Exists;
    /// this combination means to match all values and all keys
    Exists {
        key: Option<String>,
    },
    Equal {
        key: String,
        value: String,
    },
}

impl TolerationOperator {
    pub fn as_str(&self) -> &'static str {
        match self {
            Self::Exists { .. } => "Exists",
            Self::Equal { .. } => "Equal",
        }
    }

    pub fn key(&self) -> Option<&str> {
        match self {
            Self::Exists { key: Some(key) } | Self::Equal { key, .. } => Some(key),
            _ => None,
        }
    }

    pub fn value(&self) -> Option<&str> {
        if let Self::Equal { value, .. } = &self {
            Some(value)
        } else {
            None
        }
    }
}

/// Effect indicates the taint effect to match.
/// Empty means match all taint effects.
/// When specified, allowed values are
/// - NoSchedule
/// - PreferNoSchedule
/// - NoExecute
#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub enum TolerationEffect {
    NoSchedule,
    PreferNoSchedule,
    /// TolerationSeconds represents the period of time the toleration
    /// (which must be of effect NoExecute, otherwise this field is ignored) tolerates the taint.
    /// By default, it is not set, which means tolerate the taint forever (do not evict).
    /// Zero evict immediately by the system.
    NoExecute {
        seconds: u32,
    },
    /// defined as `None`
    All,
}
impl Default for TolerationEffect {
    fn default() -> Self {
        Self::All
    }
}
impl TolerationEffect {
    pub fn as_str(self) -> Option<&'static str> {
        match self {
            Self::NoSchedule => Some("NoSchedule"),
            Self::PreferNoSchedule => Some("PreferNoSchedule"),
            Self::NoExecute { .. } => Some("NoExecute"),
            Self::All => None,
        }
    }
    pub fn secs(self) -> Option<u32> {
        if let Self::NoExecute { seconds } = self {
            Some(seconds)
        } else {
            None
        }
    }
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct ContainerVolumeMount {
    pub name: String,
    pub path: PathBuf,
}

/// volumeMounts:
/// - name: prometheus-dyn
///   mountPath: /etc/targets
///
/// - name: prometheus-dyn
///   hostPath:
///   path: /var/run/prometheus
///   type: Directory
#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct DataVolume {
    pub mount: ContainerVolumeMount,
    pub host_path: PathBuf,
    pub path_type: HostPathType,
}
impl DataVolume {
    pub fn new(host_path: &str, mount_path: &str, path_type: HostPathType) -> Self {
        let host_path: PathBuf = host_path.into();
        let mount_path: PathBuf = mount_path.into();
        let file_name_fn = |path: &PathBuf| -> String {
            path.iter()
                .last()
                .map(|file_name| {
                    file_name
                        .to_string_lossy()
                        .chars()
                        .map(|c| c.is_alphabetic().then(|| c).unwrap_or('-'))
                        .collect()
                })
                .unwrap_or_default()
        };

        Self {
            mount: ContainerVolumeMount {
                name: [file_name_fn(&host_path), file_name_fn(&mount_path)].join("-"),
                path: mount_path,
            },
            host_path,
            path_type,
        }
    }
    pub fn directory_or_create(host_path: &str, mount_path: &str) -> Self {
        Self::new(host_path, mount_path, HostPathType::DirectoryOrCreate)
    }

    #[must_use]
    pub fn ns(mut self, ns: impl ToString) -> Self {
        self.host_path.push(ns.to_string());
        self
    }
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct ConfigVolume {
    pub mount: ContainerVolumeMount,

    /// mapping of relative-file-path => file-content
    pub files: HashMap<PathBuf, String>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct Environment(HashMap<String, String>);

impl std::ops::Deref for Environment {
    type Target = HashMap<String, String>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::ops::DerefMut for Environment {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl From<Environment> for HashMap<String, String> {
    fn from(env: Environment) -> Self {
        env.0
    }
}

impl From<HashMap<String, String>> for Environment {
    fn from(inner_map: HashMap<String, String>) -> Self {
        Self(inner_map)
    }
}

impl IntoIterator for Environment {
    type Item = (String, String);
    type IntoIter = <HashMap<String, String> as IntoIterator>::IntoIter;
    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn suffixes() {
        assert_eq!(suffixed(1), "1Mi");
        assert_eq!(suffixed(1024), "1Gi");
        assert_eq!(suffixed(1024 * 1024), "1Ti");
        assert_eq!(suffixed(1024 * 1024 * 1024), "1Pi");
        assert_eq!(suffixed(1024 * 1024 * 1024 * 1024), "1Ei");

        assert_eq!(suffixed(2), "2Mi");
        assert_eq!(suffixed(1025), "2Gi");
        assert_eq!(suffixed(2 * 1024), "2Gi");
        assert_eq!(suffixed(250 * 1024), "250Gi");
        assert_eq!(suffixed(1025 * 1024), "2Ti");
    }

    #[test]
    fn huge_pages() {
        let (key, value) = HugePages {
            chunk_size_mb: 1,
            quantity_mb: 1,
        }
        .format();
        assert_eq!(&key, "hugepages-1Mi");
        assert_eq!(&value, "1Mi");

        let (key, value) = HugePages {
            chunk_size_mb: 1,
            quantity_mb: 1_024,
        }
        .format();
        assert_eq!(&key, "hugepages-1Mi");
        assert_eq!(&value, "1Gi");

        let (key, value) = HugePages {
            chunk_size_mb: 2,
            quantity_mb: 1_024,
        }
        .format();
        assert_eq!(&key, "hugepages-2Mi");
        assert_eq!(&value, "1Gi");

        let (key, value) = HugePages {
            chunk_size_mb: 2,
            quantity_mb: 3 * 1_024,
        }
        .format();
        assert_eq!(&key, "hugepages-2Mi");
        assert_eq!(&value, "3Gi");
    }

    const GRAVEYARD_DEPTH: usize = WorkloadStack::<RuntimeConfig>::GRAVEYARD_DEPTH;
    const END: usize = GRAVEYARD_DEPTH + 50;

    fn build_workloads_stacks() -> (WorkloadStack<RuntimeConfig>, Vec<String>) {
        let mut active = BTreeMap::new();
        let mut container_vec = vec![];

        for order in 0..END {
            let name = uuid::Uuid::new_v4().to_string();
            container_vec.push(name.clone());
            active.insert(
                order,
                vec![RuntimeConfig {
                    environment: Environment::default(),
                    image: "IMAGE".into(),
                    name,
                    owner_reference: None,
                }],
            );
        }

        let stack = WorkloadStack {
            active,
            ..WorkloadStack::default()
        };
        (stack, container_vec)
    }

    fn batch_workloads_stacks() -> (WorkloadStack<RuntimeConfig>, Vec<String>) {
        let mut active = BTreeMap::new();
        let mut container_vec = vec![];

        for order in 0..END / 10 {
            let names: Vec<_> = std::iter::repeat_with(|| uuid::Uuid::new_v4().to_string())
                .take(10)
                .collect();
            container_vec.extend_from_slice(&names);
            active.insert(
                order,
                names
                    .into_iter()
                    .map(|name| RuntimeConfig {
                        environment: Environment::default(),
                        image: "IMAGE".into(),
                        name,
                        owner_reference: None,
                    })
                    .collect(),
            );
        }

        let stack = WorkloadStack {
            active,
            ..WorkloadStack::default()
        };
        (stack, container_vec)
    }

    fn test_workloads_stacks(mut stack: WorkloadStack<RuntimeConfig>, names: Vec<String>) {
        for name in &names {
            stack.graveyard(name);
            stack = serde_json::from_str(&serde_json::to_string(&stack).unwrap()).unwrap();
        }
        stack = serde_json::from_str(&serde_json::to_string(&stack).unwrap()).unwrap();

        assert_eq!(stack.graveyard.len(), GRAVEYARD_DEPTH);

        // the containers in graveyard are exactly the tail of `container_vec`
        let container_vec = &names[GRAVEYARD_DEPTH..END];

        for (id_a, id_b) in stack.graveyard.iter().zip(container_vec) {
            assert_eq!(id_a.get_name(), id_b)
        }
    }
    fn test_workloads_stacks_rev(mut stack: WorkloadStack<RuntimeConfig>, names: Vec<String>) {
        for name in names.iter().rev() {
            stack.graveyard(name);
            stack = serde_json::from_str(&serde_json::to_string(&stack).unwrap()).unwrap();
        }
        stack = serde_json::from_str(&serde_json::to_string(&stack).unwrap()).unwrap();

        assert_eq!(stack.graveyard.len(), GRAVEYARD_DEPTH);

        // the containers in graveyard are exactly the tail of `container_vec`
        let container_vec = &names[0..GRAVEYARD_DEPTH];

        for (id_a, id_b) in stack.graveyard.iter().rev().zip(container_vec) {
            assert_eq!(id_a.get_name(), id_b)
        }
    }
    /// validate that
    /// - `container_graveyard` maintains order after serialization
    /// - number of containers in `container_graveyard` never exceeds `HostConfig::GRAVEYARD_DEPTH`
    /// - truncated containers from `container_graveyard` are older containers
    #[test]
    fn graveyard() {
        let (stack, container_vec) = build_workloads_stacks();
        test_workloads_stacks(stack, container_vec)
    }
    #[test]
    fn revyard() {
        let (stack, container_vec) = build_workloads_stacks();
        test_workloads_stacks_rev(stack, container_vec)
    }

    #[test]
    fn batchyard() {
        let (stack, container_vec) = batch_workloads_stacks();
        test_workloads_stacks(stack, container_vec)
    }

    #[test]
    fn revbatchyard() {
        let (stack, container_vec) = batch_workloads_stacks();
        test_workloads_stacks_rev(stack, container_vec)
    }
}
