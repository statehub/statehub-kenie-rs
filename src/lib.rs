//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

#![cfg_attr(feature = "pedantic", warn(clippy::pedantic))]
#![warn(clippy::use_self)]
#![warn(clippy::map_flatten)]
#![warn(clippy::map_unwrap_or)]
#![warn(deprecated_in_future)]
#![warn(future_incompatible)]
#![warn(noop_method_call)]
#![warn(unreachable_pub)]
#![warn(missing_debug_implementations)]
#![warn(rust_2018_compatibility)]
#![warn(rust_2021_compatibility)]
#![warn(rust_2018_idioms)]
#![warn(unused)]
#![deny(warnings)]

pub use statehub_k8s_helper as k8s;

#[cfg(feature = "controller")]
pub mod controller;
#[cfg(feature = "kubectl")]
pub mod kubectl;
pub mod spec;
#[cfg(any(feature = "controller", feature = "kubectl"))]
pub mod utils;

/// runtime-environment-variables exposed to pods
#[cfg(any(feature = "controller", feature = "kubectl"))]
pub mod env {
    use crate::k8s::corev1::EnvVar;
    pub const POD_UID: &str = "POD_UID";
    pub const POD_IP: &str = "POD_IP";
    pub const POD_NAME: &str = "POD_NAME";
    pub const NAMESPACE: &str = "NAMESPACE";
    pub const NODE_NAME: &str = "NODE_NAME";
    pub const HOST_IP: &str = "HOST_IP";

    pub fn all() -> [EnvVar; 6] {
        use statehub_k8s_helper::EnvVarExt;
        [
            EnvVar::spec_nodename(NODE_NAME),
            EnvVar::status_host_ip(HOST_IP),
            EnvVar::metadata_uid(POD_UID),
            EnvVar::metadata_name(POD_NAME),
            EnvVar::status_pod_ip(POD_IP),
            EnvVar::metadata_namespace(NAMESPACE),
        ]
    }
}
