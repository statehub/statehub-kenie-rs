//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::collections::BTreeMap;
use std::fmt::Debug;
use std::hash::Hash;
use std::marker::Unpin;
use std::sync::Arc;
use std::time::Duration;

use futures::StreamExt;
use itertools::Itertools;
use thiserror::Error;

use kube::core::object::HasSpec;
use kube::core::object::HasStatus;
use kube::core::params::PatchParams;
use kube::runtime::controller;
use kube::runtime::finalizer;
use kube::runtime::reflector::ObjectRef;
use kube::CustomResourceExt;
use kube::{api, Resource};
use serde::{de, ser};

mod deployment;
pub use deployment::*;

pub const CRD_SPEC_HASH: &str = "SPEC_HASH";
pub const CRD_API_VERSION: &str = "CRD_API_VERSION";
pub const CRD_KIND: &str = "CRD_KIND";
pub const CRD_PARENT_SPEC_HASH: &str = "PARENT_SPEC_HASH";
pub const CRD_DELETION_TIMESTAMP: &str = "DELETION_TIMESTAMP";
pub const REQUEST_ID: &str = "REQUEST_ID";

#[async_trait::async_trait]
pub trait OpinionatedController: Clone + Sized + Send + Sync {
    /// Kubernetes resource this controller is taking care of
    type K: kube::Resource
        + CustomResourceExt
        + HasStatus
        + HasSpec
        + ser::Serialize
        + de::DeserializeOwned
        + Clone
        + Send
        + Sync
        + Debug
        + 'static;
    /// Your error type
    type Error: std::error::Error + Send + Sync + From<kube::Error> + 'static;

    /// Controller name used in various objects
    const CONTROLLER_NAME: &'static str;
    const MIN_BACKOFF_SECS: u64 = 1;
    const MAX_BACKOFF_SECS: u64 = 1_000;

    fn timeout(&self, _obj: &Self::K) -> Option<Duration> {
        Some(Duration::from_secs(1_000))
    }

    fn patch_params() -> PatchParams {
        PatchParams::apply(Self::CONTROLLER_NAME)
    }

    /// `kube::Api` object for manipulating `K` resources
    fn api(&self) -> api::Api<Self::K>;

    fn helper(&self) -> crate::k8s::Kubectl {
        crate::k8s::Kubectl::from_client(self.api().into_client()).manager(Self::CONTROLLER_NAME)
    }

    /// Event dispatcher - by default simply calls `apply` and `cleanup` methods
    async fn dispatch(
        &self,
        event: finalizer::Event<Self::K>,
    ) -> Result<controller::Action, Self::Error> {
        use controller::Action;
        match event {
            finalizer::Event::Apply(object) => self.apply(object).await,
            finalizer::Event::Cleanup(object) => self.cleanup(object).await,
        }
        .map(|action| action.map_or_else(Action::await_change, Action::requeue))
    }

    /// after reconcile loop ended - patch `K::Status` with a new one  
    async fn update_status(
        &self,
        name: &str,
        status: <Self::K as HasStatus>::Status,
    ) -> kube::Result<Self::K>
    where
        <Self::K as kube::Resource>::DynamicType: Default,
        <Self::K as HasStatus>::Status: serde::Serialize + Send,
    {
        use kube::api::Patch::Apply;

        let api = &self.api();
        let pp = &Self::patch_params();

        let d = <Self::K as kube::Resource>::DynamicType::default();
        let kind = Self::K::kind(&d);
        let api_version = Self::K::api_version(&d);

        api.patch_status(
            name,
            pp,
            &Apply(serde_json::json!({
                "apiVersion": api_version,
                "kind": kind,
                "status": status,
            })),
        )
        .await
    }

    /// Called when new object is created or changes are made to the existing object
    async fn apply(&self, object: Arc<Self::K>) -> Result<Option<Duration>, Self::Error>;

    /// Called when object is deleted
    async fn cleanup(&self, object: Arc<Self::K>) -> Result<Option<Duration>, Self::Error>;

    /// Handles reconciler errors
    fn error_policy(&self, error: &finalizer::Error<Self::Error>) -> Option<Duration> {
        tracing::error!("reconciler error: {}", error);

        matches!(error, finalizer::Error::UnnamedObject).then(|| Duration::from_secs(60))
    }

    /// Returns the finalizer name to be applied to all the objects
    fn finalizer_name(&self) -> String {
        format!("{}/cleanup", Self::CONTROLLER_NAME)
    }

    async fn reconciler(
        &self,
        obj: Arc<Self::K>,
    ) -> Result<controller::Action, finalizer::Error<Self::Error>> {
        let api = self.api();
        let finalizer_name = self.finalizer_name();
        let reconcile = |event| self.dispatch(event);

        finalizer::finalizer(&api, &finalizer_name, obj, reconcile).await
    }

    /// The name of the controller pod can be retrieved using Kubernetes' API or it can be injected as an environment variable using
    /// env:
    ///   - name: POD_NAME
    ///     valueFrom:
    ///       fieldRef:
    ///         fieldPath: metadata.name
    /// use corev1::EnvVar::metadata_name("POD_NAME")
    fn make_event<U, E>(k: &Self::K, result: Option<&Result<U, E>>) -> crate::k8s::corev1::Event
    where
        <Self::K as kube::Resource>::DynamicType: Default,
        Self::K: HasSpec,
        <Self::K as HasSpec>::Spec: Hash + serde::Serialize,
        E: std::fmt::Display,
    {
        use crate::env::POD_NAME;
        use crate::k8s::corev1::EventSource;
        use crate::k8s::metav1::ObjectMeta;
        use crate::k8s::openapi::apimachinery::pkg::apis::meta::v1::MicroTime;
        use once_cell::sync::Lazy;

        static INSTANCE: Lazy<Option<String>> = Lazy::new(|| {
            let instance = std::env::var(POD_NAME);
            if let Err(e) = &instance {
                tracing::warn!("{} {}", POD_NAME, e)
            }
            instance.ok()
        });

        let dt = <Self::K as kube::Resource>::DynamicType::default();
        let object_ref = k.object_ref(&dt);
        let object_ref = statehub_k8s_helper::corev1::ObjectReference {
            namespace: Some(
                object_ref
                    .namespace
                    .unwrap_or_else(|| "default".to_string()),
            ),
            ..object_ref
        };
        let generate_name = std::iter::once(slug::slugify(Self::CONTROLLER_NAME))
            .chain(object_ref.name.clone())
            .chain(object_ref.resource_version.clone())
            .join("-");

        let labels = Some(event_labels(k));

        let annotations = k
            .meta()
            .deletion_timestamp
            .as_ref()
            .map(|dt| maplit::btreemap!(CRD_DELETION_TIMESTAMP.into() => dt.0.to_string()));

        let controller_name = Self::CONTROLLER_NAME;
        let instance = INSTANCE.clone();

        let action = "Reconcile".to_string();

        let err = result
            .and_then(|result| result.as_ref().err())
            .map(ToString::to_string);

        let reason = reason_from_result(result).to_string();

        // taken from runtime::event.rs
        let ty = if err.is_some() { "Warning" } else { "Normal" };

        crate::k8s::corev1::Event {
            action: Some(action),
            reason: Some(reason),
            event_time: Some(MicroTime(chrono::Utc::now())),
            metadata: ObjectMeta {
                namespace: object_ref.namespace.clone(),
                generate_name: Some(generate_name),
                labels,
                annotations,
                ..Default::default()
            },
            source: Some(EventSource {
                component: Some(controller_name.into()),
                host: Some(instance.clone().unwrap_or_else(|| controller_name.into())),
            }),
            reporting_component: Some(controller_name.into()),
            reporting_instance: Some(instance.unwrap_or_else(|| controller_name.into())),
            series: None,
            type_: Some(ty.into()),
            related: None,
            count: None,
            first_timestamp: None,
            involved_object: object_ref,
            last_timestamp: None,
            message: SpecWithErr {
                spec: k.spec(),
                err,
            }
            .message(),
        }
    }

    async fn emit_event(&self, k: &Self::K, result: Option<&ResultCtx<Self>>) -> kube::Result<()>
    where
        <Self::K as kube::Resource>::DynamicType: Default,
        <Self::K as HasSpec>::Spec: Hash + serde::Serialize,
    {
        use kube::core::params::PostParams;
        match result {
            // if we request a requeue, or timed-out -
            // then don't emit an event - so we don't pollute the event
            Some(Err(ControllerCtxError::Timeout { .. })) => Ok(()),
            _ => {
                let event = Self::make_event(k, result);
                let api = self.api().into();
                let api = match &event.metadata.namespace {
                    Some(ns) => api::Api::namespaced(api, ns),
                    None => api::Api::default_namespaced(api),
                };
                api.create(&PostParams::default(), &event).await?;
                Ok(())
            }
        }
    }

    /// set child cr with `k.spec` hash and `k.meta.labels.request_id` if not None
    fn label_child<C>(k: &Self::K, mut child: C) -> C
    where
        C: kube::Resource,
        <Self::K as HasSpec>::Spec: Hash,
    {
        use kube::ResourceExt;
        let labels = child.labels_mut();
        let key = REQUEST_ID.to_string();
        if let Some(request_id) = k.meta().labels.as_ref().and_then(|l| l.get(&key)) {
            labels.insert(key, request_id.clone());
        }

        labels.insert(CRD_PARENT_SPEC_HASH.into(), hash_spec(k).to_string());

        child
    }
}

#[derive(serde::Serialize)]
struct SpecWithErr<'a, SPEC: serde::Serialize + ?Sized> {
    spec: &'a SPEC,
    #[serde(skip_serializing_if = "Option::is_none")]
    err: Option<String>,
}

impl<'a, SPEC: serde::Serialize + ?Sized> SpecWithErr<'a, SPEC> {
    /// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.22/#event-v1-events-k8s-io
    /// note is a human-readable description of the status of this operation.
    /// Maximal length of the note is 1kB,
    /// but libraries should be prepared to handle values up to 64kB.
    const MAX_LEN: usize = 1_024;
    fn message(self) -> Option<String> {
        match serde_json::to_string(&self) {
            Ok(msg) if msg.len() <= Self::MAX_LEN => Some(msg),
            _ => {
                match serde_json::to_string(&SpecWithErr::<'static, str> {
                    spec: "I would write the Spec but the margin is too narrow to contain it",
                    err: self.err.clone(),
                }) {
                    Ok(msg) if msg.len() <= Self::MAX_LEN => Some(msg),
                    _ => self.err.map(|mut e| {
                        e.truncate(Self::MAX_LEN);
                        e
                    }),
                }
            }
        }
    }
}

/// get the event::reason from the reconcile result
fn reason_from_result<U, E>(result: Option<&Result<U, E>>) -> &'static str {
    match result {
        None => "Reconcile Started",
        Some(Ok(_)) => "Reconcile Finished",
        Some(Err(_)) => "Reconcile Failed",
    }
}

/// get a k.specs hash values
fn hash_spec<K: Resource + HasSpec>(k: &K) -> u64
where
    K::Spec: Hash,
{
    use std::hash::Hasher;
    let mut hasher = std::collections::hash_map::DefaultHasher::new();
    k.spec().hash(&mut hasher);
    hasher.finish()
}

/// get and `event` labels extracted from as `resource` (namely - a `CRD`)
fn event_labels<K: Resource + HasSpec>(k: &K) -> BTreeMap<String, String>
where
    K::Spec: Hash,
    <K as kube::Resource>::DynamicType: Default,
{
    use maplit::{btreemap, convert_args};
    let spec_hash = hash_spec(k).to_string();
    let dt = K::DynamicType::default();
    let api_version = slug::slugify(K::api_version(&dt));
    let kind = slug::slugify(K::kind(&dt));

    let mut labels = convert_args!(
        btreemap!(CRD_SPEC_HASH => spec_hash, CRD_API_VERSION => api_version, CRD_KIND => kind)
    );
    let k_labels = k.meta().labels.as_ref();

    if let Some(parent_spec_hash) = k_labels.and_then(|l| l.get(CRD_PARENT_SPEC_HASH)) {
        labels.insert(CRD_PARENT_SPEC_HASH.into(), parent_spec_hash.clone());
    }

    if let Some(request_id) = k_labels.and_then(|l| l.get(REQUEST_ID)) {
        labels.insert(REQUEST_ID.into(), request_id.clone());
    }

    labels
}

type ObjRefStore<K, V> = parking_lot::Mutex<ahash::AHashMap<ObjectRef<K>, V>>;

#[derive(Debug)]
pub struct ControllerCtx<T: OpinionatedController>
where
    <T::K as kube::Resource>::DynamicType: Default + Debug + Hash + Eq,
{
    pub controller: T,
    error_count: ObjRefStore<T::K, u32>,
}

#[derive(Debug, Error)]
pub enum ControllerCtxError<T: OpinionatedController> {
    #[error("{:?}", error)]
    Reconcile {
        count: u32,
        #[source]
        error: finalizer::Error<T::Error>,
    },
    #[error("reconcile timed out")]
    Timeout { count: u32 },
}
pub type ResultCtx<T> = Result<controller::Action, ControllerCtxError<T>>;

impl<T: OpinionatedController> ControllerCtx<T>
where
    <T::K as kube::Resource>::DynamicType: Default + Debug + Hash + Eq,
{
    fn reconciler_failed(&self, obj_ref: ObjectRef<T::K>) -> u32 {
        let mut error_count = self.error_count.lock();
        let counter = error_count.entry(obj_ref).or_default();
        let count = *counter;
        *counter += 1;
        count
    }

    pub async fn reconciler(obj: Arc<T::K>, ctx: controller::Context<Self>) -> ResultCtx<T>
    where
        T: OpinionatedController + 'static,
        <T::K as HasSpec>::Spec: Hash + serde::Serialize,
    {
        let ctx = ctx.get_ref();
        let controller = &ctx.controller;

        let obj_ref = ObjectRef::from_obj(obj.as_ref());
        let object = Arc::clone(&obj);

        if let Err(e) = controller.emit_event(&object, None).await {
            tracing::warn!(?e, "failed to emit-event")
        }

        let reconcile_result = if let Some(timeout) = controller.timeout(&obj) {
            tokio::time::timeout(timeout, controller.reconciler(obj)).await
        } else {
            Ok(controller.reconciler(obj).await)
        };

        let count = match &reconcile_result {
            Ok(Err(_)) | Err(_) => ctx.reconciler_failed(obj_ref),
            _ => {
                ctx.error_count.lock().remove(&obj_ref);
                0
            }
        };

        let result = match reconcile_result {
            Ok(Ok(action)) => Ok(action),
            Ok(Err(error)) => Err(ControllerCtxError::Reconcile { count, error }),
            Err(_) => Err(ControllerCtxError::Timeout { count }),
        };

        if let Err(e) = controller.emit_event(&object, Some(&result)).await {
            tracing::warn!(?e, "failed to emit-event")
        }
        result
    }

    pub fn error_policy(
        error: &ControllerCtxError<T>,
        ctx: controller::Context<Self>,
    ) -> controller::Action {
        let (ControllerCtxError::Reconcile { count, .. } | ControllerCtxError::Timeout { count }) =
            error;

        // pow2
        let backoff = 1_u64.checked_shl(*count).unwrap_or(T::MAX_BACKOFF_SECS);
        let backoff = backoff.min(T::MAX_BACKOFF_SECS);
        let backoff = backoff.max(T::MIN_BACKOFF_SECS);
        let requeue_after = Some(Duration::from_secs(backoff));

        let action = if let ControllerCtxError::Reconcile { error, .. } = error {
            ctx.get_ref().controller.error_policy(error)
        } else {
            None
        };

        action.max(requeue_after).map_or_else(
            controller::Action::await_change,
            controller::Action::requeue,
        )
    }
}

impl<T: OpinionatedController> ControllerCtx<T>
where
    <T::K as kube::Resource>::DynamicType: Default + Debug + Hash + Eq,
{
    pub fn new(controller: T) -> controller::Context<Self> {
        controller::Context::new(Self {
            controller,
            error_count: parking_lot::Mutex::new(ahash::AHashMap::new()),
        })
    }

    pub async fn run(controller: controller::Controller<T::K>, ctx: T) -> Result<(), T::Error>
    where
        T: OpinionatedController + Debug + 'static,
        <T::K as kube::Resource>::DynamicType: Default + Clone + Unpin,
        <T::K as HasSpec>::Spec: Hash + ser::Serialize,
    {
        controller
            .shutdown_on_signal()
            .run(Self::reconciler, Self::error_policy, Self::new(ctx))
            .for_each(|result| async move {
                let name = T::CONTROLLER_NAME;
                match result {
                    Ok((object, action)) => {
                        tracing::info!(name, "reconciled {} ({:?})", object, action)
                    }
                    Err(err) => tracing::warn!(name, "reconcile failed: {:?}", err),
                }
            })
            .await;

        tracing::info!("controller terminated");
        Ok(())
    }
}

pub async fn controller<T>(ctx: T, lp: api::ListParams) -> Result<(), T::Error>
where
    T: OpinionatedController + Debug + 'static,
    <T::K as kube::Resource>::DynamicType: Hash + Eq + Default + Debug + Clone + Unpin,
    <T::K as HasSpec>::Spec: Hash + serde::Serialize,
{
    ControllerCtx::run(controller::Controller::new(ctx.api(), lp), ctx).await
}

#[cfg(test)]
pub(crate) mod test_utils {
    pub(crate) use super::*;
    pub(crate) use kube::CustomResource;
    pub(crate) use kube::CustomResourceExt;
    pub(crate) use schemars::JsonSchema;
    pub(crate) use serde::{Deserialize, Serialize};

    pub(crate) fn init_tracing_subscriber() {
        use tracing_subscriber::fmt;
        let _ = tracing_subscriber::fmt()
            .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
            .pretty()
            .with_timer(fmt::time::UtcTime::rfc_3339())
            .try_init();
    }
    #[derive(Clone, Debug, Serialize, Deserialize, JsonSchema, PartialEq, Eq, Hash)]
    pub(crate) struct IdStatus {
        pub(crate) id: uuid::Uuid,
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use futures::StreamExt;
    use std::hash::Hash;
    use std::sync::Arc;
    use std::time::Duration;

    use k8s::apiextensionsv1::CustomResourceDefinition;
    use k8s::metav1::ObjectMeta;
    use k8s::Kubectl;
    use kube::api::WatchEvent;
    use kube::runtime::wait;
    use kube::{Api, Resource, ResourceExt};
    use statehub_k8s_helper as k8s;
    use wait::delete::delete_and_finalize;

    use super::test_utils::*;

    #[derive(
        Clone, Debug, Serialize, Deserialize, CustomResource, JsonSchema, PartialEq, Eq, Hash,
    )]
    #[kube(
        group = "statehub.cloud",
        version = "v1alpha1",
        kind = "IdCrd",
        status = "IdStatus",
        shortname = "id",
        crates(k8s_openapi = "k8s::openapi")
    )]
    struct IdSpec {
        id: uuid::Uuid,
    }

    #[derive(Clone, Debug)]
    struct IdManager {
        client: Kubectl,
    }

    #[async_trait::async_trait]
    impl super::OpinionatedController for IdManager {
        type K = IdCrd;
        type Error = kube::Error;
        const CONTROLLER_NAME: &'static str = "id-manager";

        fn api(&self) -> Api<Self::K> {
            self.client.api()
        }

        async fn apply(&self, object: Arc<Self::K>) -> Result<Option<Duration>, Self::Error> {
            let id: uuid::Uuid = object.spec.id;
            let name = &object.name();
            self.update_status(name, IdStatus { id }).await?;

            Ok(None)
        }

        async fn cleanup(&self, _x: Arc<Self::K>) -> Result<Option<Duration>, Self::Error> {
            Ok(None)
        }
    }

    #[tokio::test]
    #[ignore]
    async fn basic_reconcile() {
        init_tracing_subscriber();
        let client = Kubectl::try_default().await.unwrap();
        let manager = IdManager {
            client: client.clone(),
        };

        let spec = IdSpec {
            id: uuid::Uuid::new_v4(),
        };

        let id = spec.id;
        let name = id.to_string();

        client.ensure_crd_is_installed(IdCrd::crd()).await.unwrap();

        let lp = &api::ListParams::default().fields(&format!("metadata.name={}", name));
        tokio::spawn(super::controller(manager, lp.clone()));

        let api = client.api();
        let id_crd = client
            .patch(
                IdCrd {
                    metadata: ObjectMeta {
                        name: Some(name.clone()),
                        ..ObjectMeta::default()
                    },
                    spec,
                    status: None,
                },
                api.clone(),
            )
            .await
            .unwrap();

        let resource_version = id_crd.meta().resource_version.as_deref().unwrap_or("0");

        api.watch(lp, resource_version)
            .await
            .unwrap()
            .take_while(|event| {
                let status = match event.as_ref().unwrap() {
                    // keep waiting for status to be set
                    WatchEvent::Added(event) | WatchEvent::Modified(event) => event.status.as_ref(),
                    WatchEvent::Bookmark(_) => None,
                    WatchEvent::Deleted(s) => panic!("Deleted {}", s.name()),
                    WatchEvent::Error(s) => panic!("{}", s),
                };
                if let Some(status) = status {
                    assert_eq!(status.id, id);
                }
                let continue_to_watch = status.is_none();
                async move { continue_to_watch }
            })
            .for_each(|_| async {})
            .await;

        let dp = &kube::api::DeleteParams {
            propagation_policy: Some(kube::api::PropagationPolicy::Foreground),
            ..kube::api::DeleteParams::default()
        };

        delete_and_finalize(api, &name, dp)
            .await
            .or_else(|e| {
                matches!(&e, wait::delete::Error::Delete(kube::Error::Api(e)) if e.code == 404)
                    .then(|| ())
                    .ok_or(e)
            })
            .unwrap();

        let label_selector = event_labels(&id_crd)
            .iter()
            .map(|(key, value)| format!("{key}={value}"))
            .join(",");

        let event_lp = &api::ListParams::default().labels(&label_selector);

        client
            .api::<crate::k8s::corev1::Event>()
            .watch(event_lp, "0")
            .await
            .unwrap()
            .filter_map(|event| {
                let event = match event {
                    // keep waiting for status to be set
                    Ok(WatchEvent::Added(event) | WatchEvent::Modified(event)) => Some(event),
                    Ok(WatchEvent::Bookmark(_) | WatchEvent::Deleted(_) | WatchEvent::Error(_))
                    | Err(_) => {
                        panic!("failed to watch events")
                    }
                };
                async move { event }
            })
            .take_while(|event| {
                let not_deleted = !event
                    .annotations()
                    .contains_key(super::CRD_DELETION_TIMESTAMP);
                let not_finished =
                    event.reason.as_deref() != Some(reason_from_result::<_, ()>(Some(&Ok(()))));
                let take = not_finished || not_deleted;
                async move { take }
            })
            .for_each(|_| async {})
            .await;

        client
            .api::<CustomResourceDefinition>()
            .delete(&IdCrd::crd().name(), dp)
            .await
            .unwrap();
    }

    #[derive(
        Copy, Clone, Debug, Serialize, Deserialize, CustomResource, JsonSchema, PartialEq, Eq, Hash,
    )]
    #[kube(
        group = "statehub.cloud",
        version = "v1alpha1",
        kind = "IdCrdFail",
        struct = "IdCrdFailA",
        status = "IdStatus",
        shortname = "idf",
        crates(k8s_openapi = "k8s::openapi")
    )]
    struct IdSpecA {
        id_a: uuid::Uuid,
    }

    #[derive(
        Copy, Clone, Debug, Serialize, Deserialize, CustomResource, JsonSchema, PartialEq, Eq, Hash,
    )]
    #[kube(
        group = "statehub.cloud",
        version = "v1alpha1",
        kind = "IdCrdFail",
        struct = "IdCrdFailB",
        status = "IdStatus",
        shortname = "idf",
        crates(k8s_openapi = "k8s::openapi")
    )]
    struct IdSpecB {
        id_b: uuid::Uuid,
    }

    /// since `id_b` field name is different then `id_a` `IdSpecB` and `IdSpecA` should conflict
    /// whereas calling
    /// ```
    /// ensure_crd_is_installed(IdCrdFailA::crd())
    /// ```
    /// twice should be find
    #[tokio::test]
    #[ignore]
    async fn conflicting_crds() {
        init_tracing_subscriber();
        let client = Kubectl::try_default().await.unwrap();
        client
            .ensure_crd_is_installed(IdCrdFailA::crd())
            .await
            .unwrap();
        client
            .ensure_crd_is_installed(IdCrdFailA::crd())
            .await
            .unwrap();
        client
            .ensure_crd_is_installed(IdCrdFailB::crd())
            .await
            .expect_err("IdCrdFailA should conflict with IdCrdFailB");

        let dp = &kube::api::DeleteParams {
            propagation_policy: Some(kube::api::PropagationPolicy::Foreground),
            ..kube::api::DeleteParams::default()
        };

        delete_and_finalize(
            client.api::<CustomResourceDefinition>(),
            &IdCrdFailA::crd().name(),
            dp,
        )
        .await
        .or_else(|e| {
            matches!(&e, wait::delete::Error::Delete(kube::Error::Api(e)) if e.code == 404)
                .then(|| ())
                .ok_or(e)
        })
        .unwrap();

        client
            .ensure_crd_is_installed(IdCrdFailB::crd())
            .await
            .unwrap();

        delete_and_finalize(
            client.api::<CustomResourceDefinition>(),
            &IdCrdFailB::crd().name(),
            dp,
        )
        .await
        .or_else(|e| {
            matches!(&e, wait::delete::Error::Delete(kube::Error::Api(e)) if e.code == 404)
                .then(|| ())
                .ok_or(e)
        })
        .unwrap();
    }
}
