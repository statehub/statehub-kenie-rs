//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::fmt;
use std::fmt::Debug;
use std::net::IpAddr;

use appsv1::StatefulSet;

use super::k8s::openapi::api::networking::v1::Ingress;
use super::k8s::openapi::Resource;
use anyhow::Context;
use apiextensionsv1::CustomResourceDefinition;
use appsv1::DaemonSet;
use appsv1::Deployment;
use futures::future::try_join_all;
use kube::core::params::PatchParams;
use kube::core::Resource as _;
use kube::runtime::wait::conditions::is_crd_established;
use kube::runtime::wait::Condition;
use kube::ResourceExt;
use metav1::ObjectMeta;
use serde_yaml::to_writer;

use super::k8s::apiextensionsv1;
use super::k8s::appsv1;
use super::k8s::batchv1;
use super::k8s::corev1;
use super::k8s::metav1;
use super::k8s::rbacv1;

use crate::spec::DeploymentKind;
use crate::spec::RuntimeConfig;
use crate::spec::Spec;
use crate::spec::WorkloadRef;
use crate::spec::WorkloadStack;

pub use crate::utils::create_job;
use crate::utils::delete_and_await;
use crate::utils::{OrAlreadyExists, OrNotFound, TraceResult, WithOption};

mod transform;
pub use super::env::*;
pub use transform::secret;
use transform::*;

const API_GROUPS: &str = "rbac.authorization.k8s.io";

#[derive(Debug)]
pub struct Kubectl {
    client: crate::k8s::Kubectl,
    namespace: String,
    service_account: String,
}

#[derive(Clone, Debug)]
pub struct KubeApi<K> {
    api: kube::Api<K>,
    field_manger: String,
}

impl<K: Clone + serde::de::DeserializeOwned + serde::Serialize + Debug> KubeApi<K> {
    pub async fn create(&self, k: K) -> anyhow::Result<K> {
        let pp = kube::api::PostParams::default();
        self.api.create(&pp, &k).await.or_already_exists(k)
    }
}

impl<K> KubeApi<K>
where
    K: Debug + Clone + serde::de::DeserializeOwned + serde::Serialize + ResourceExt,
{
    pub async fn apply(&self, item: K) -> anyhow::Result<K> {
        let name = item.meta().name.as_ref().context("no name was given")?;
        let pp = kube::api::PatchParams::apply(&self.field_manger);
        self.api
            .patch(name, &pp, &kube::api::Patch::Apply(&item))
            .await
            .with_context(|| format!("apply {:100?}", item))
            .trace()
    }

    pub async fn annotate(&self, obj_name: &str, key: String, value: String) -> kube::Result<K>
    where
        <K as kube::core::Resource>::DynamicType: Default,
    {
        let mut obj: K = self.get(obj_name).await?;
        obj.annotations_mut().insert(key, value);
        self.api
            .patch(
                obj_name,
                &PatchParams::apply(&self.field_manger),
                &kube::api::Patch::Apply(obj),
            )
            .await
    }
}

impl<K> KubeApi<K>
where
    K: serde::de::DeserializeOwned,
{
    async fn get_status(&self, name: &str) -> kube::Result<K> {
        self.api.get_status(name).await
    }
}

impl<K> KubeApi<K>
where
    K: Clone + serde::de::DeserializeOwned + Debug + serde::Serialize,
{
    async fn get(&self, name: &str) -> kube::Result<K> {
        self.api.get(name).await
    }

    async fn delete(&self, name: &str) -> anyhow::Result<()> {
        let dp = kube::api::DeleteParams {
            propagation_policy: Some(kube::api::PropagationPolicy::Foreground),
            ..kube::api::DeleteParams::default()
        };

        self.api.delete(name, &dp).await.or_not_found()?;

        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct KubeConfig(kube::Config);
impl KubeConfig {
    /// # Errors
    ///
    /// fail to infer kube-config
    pub async fn new() -> anyhow::Result<Self> {
        Ok(Self(kube::Config::infer().await?))
    }

    /// # Errors
    ///
    /// fail to convert kube-config into kube-client
    pub fn client(&self, ns: impl Into<Option<String>>) -> kube::Result<Kubectl> {
        Kubectl::new(self.0.clone(), ns)
    }
}

impl Kubectl {
    pub async fn setup(&self, stack: &WorkloadStack<WorkloadRef<'_>>) -> anyhow::Result<()> {
        stack.sanitize()?;
        for workloads in stack.active.clone().into_values() {
            try_join_all(
                workloads
                    .iter()
                    .map(|workload| self.apply(workload.config, workload.spec)),
            )
            .await?;
        }

        Ok(())
    }

    pub async fn graveyard(&self, stack: &WorkloadStack<WorkloadRef<'_>>) -> anyhow::Result<()> {
        try_join_all(stack.graveyard.iter().map(|workload| async move {
            let config = workload.config;
            tracing::info!("delete {}", config.name);
            self.delete(config, workload.spec).await
        }))
        .await?;

        Ok(())
    }
}

impl Kubectl {
    /// # Errors
    ///
    /// fail to create default client
    fn new(config: kube::Config, ns: impl Into<Option<String>>) -> kube::Result<Self> {
        let namespace = ns
            .into()
            .unwrap_or_else(|| config.default_namespace.clone());

        Ok(Self {
            client: crate::k8s::Kubectl::from_config(config)?,
            service_account: transform::service_account(&namespace),
            namespace,
        })
    }

    pub fn from_helper(
        client: crate::k8s::Kubectl,
        namespace: impl Into<Option<String>>,
        service_account: String,
    ) -> Self {
        Self {
            client,
            namespace: namespace.into().unwrap_or_else(|| "default".into()),
            service_account,
        }
    }

    pub fn with_client(
        client: kube::Client,
        namespace: String,
        service_account: String,
        field_manager: impl Into<Option<String>>,
    ) -> Self {
        let client = if let Some(field_manager) = field_manager.into() {
            crate::k8s::Kubectl::from_client(client).manager(&field_manager)
        } else {
            crate::k8s::Kubectl::from_client(client)
        };
        Self::from_helper(client, namespace, service_account)
    }

    /// # Errors
    ///
    /// - fail to infer kube-config
    /// - fail to convert kube-config into kube-client
    pub async fn client(ns: impl Into<Option<String>>) -> anyhow::Result<Self> {
        KubeConfig::new().await?.client(ns).context("create client")
    }

    pub fn api<K: kube::core::Resource>(&self) -> KubeApi<K>
    where
        <K as kube::core::Resource>::DynamicType: Default,
    {
        KubeApi {
            api: kube::api::Api::namespaced(self.client.client(), &self.namespace),
            field_manger: self.client.get_manager().to_string(),
        }
    }

    pub fn api_all<K: kube::core::Resource>(&self) -> KubeApi<K>
    where
        <K as kube::core::Resource>::DynamicType: Default,
    {
        KubeApi {
            api: kube::api::Api::all(self.client.client()),
            field_manger: self.client.get_manager().to_string(),
        }
    }

    pub fn api_def<K: kube::core::Resource>(&self) -> KubeApi<K>
    where
        <K as kube::core::Resource>::DynamicType: Default,
    {
        KubeApi {
            api: kube::api::Api::default_namespaced(self.client.client()),
            field_manger: self.client.get_manager().to_string(),
        }
    }

    /// # Errors
    ///
    /// fail to create client from file in `path`
    pub async fn with_config(
        path: impl AsRef<std::path::Path>,
        namespace: impl AsRef<str>,
        field_manager: impl Into<Option<String>>,
    ) -> anyhow::Result<Self> {
        let config = kube::config::Kubeconfig::read_from(path)?;
        let options = kube::config::KubeConfigOptions::default();

        let config = kube::config::Config::from_custom_kubeconfig(config, &options).await?;
        let client = if let Some(field_manager) = field_manager.into() {
            crate::k8s::Kubectl::from_config(config)?.manager(&field_manager)
        } else {
            crate::k8s::Kubectl::from_config(config)?
        };
        Ok(Self {
            client,
            namespace: namespace.as_ref().into(),
            service_account: transform::service_account(namespace),
        })
    }

    /// # Errors
    ///
    /// fail to create one of
    /// - service-account
    /// - cluster-role
    /// - cluster-role-binding
    /// - fail to get `image-pull-secret` from default namespace
    ///
    pub async fn setup_cluster_account(
        &self,
        role_name: &str,
        image_pull_secret: &str,
    ) -> anyhow::Result<()> {
        let (ns, service_account, cluster_role, role_binding) = cluster_account(
            role_name,
            &self.service_account,
            image_pull_secret,
            &self.namespace,
        );
        self.api_all().create(ns).await?;

        self.api_all().create(cluster_role).await?;

        self.api().create(service_account).await?;

        self.api_all().create(role_binding).await?;

        self.copy_secret(image_pull_secret).await
    }

    async fn copy_secret(&self, image_pull_secret: &str) -> anyhow::Result<()> {
        let secret = self
            .api_def::<corev1::Secret>()
            .get(image_pull_secret)
            .await?;

        let metadata = ObjectMeta {
            name: secret.metadata.name,
            namespace: Some(self.namespace.clone()),
            annotations: secret.metadata.annotations,
            labels: secret.metadata.labels,
            ..ObjectMeta::default()
        };

        self.api()
            .create(corev1::Secret { metadata, ..secret })
            .await?;

        Ok(())
    }

    pub async fn namespace_with_pull_secret(&self, image_pull_secret: &str) -> anyhow::Result<()> {
        let ns = mgmt_namespace(&self.namespace);
        self.api_all().create(ns).await?;
        self.copy_secret(image_pull_secret).await
    }

    /// # Errors
    ///
    /// fail to create one of
    /// - service-account
    /// - role
    /// - role-binding
    /// - fail to get `image-pull-secret` from default namespace
    pub async fn setup_account(
        &self,
        role_name: &str,
        image_pull_secret: &str,
    ) -> anyhow::Result<()> {
        let (service_account, role, role_binding) = account(
            role_name,
            &self.service_account,
            image_pull_secret,
            &self.namespace,
        );

        self.api().create(role).await?;

        self.api().create(service_account).await?;

        self.api().create(role_binding).await?;

        Ok(())
    }

    /// # Errors
    ///
    /// fail to find create crd
    pub async fn create_crd<K>(
        &self,
        cr: K,
        group: &str,
        version: &str,
        kind: &str,
    ) -> anyhow::Result<K>
    where
        K: serde::Serialize + serde::de::DeserializeOwned + fmt::Debug,
    {
        let pp = kube::api::PostParams::default();

        let data = serde_json::to_vec(&cr).context("failed to serialize custom resources")?;
        let gvk = kube::core::gvk::GroupVersionKind::gvk(group, version, kind);
        let res = kube::core::discovery::ApiResource::from_gvk(&gvk);
        let url = kube::core::DynamicObject::url_path(&res, Some(&self.namespace));
        let request = kube::core::Request::new(url)
            .create(&pp, data)
            .context("failed to create request")?;
        self.client
            .client()
            .request::<K>(request)
            .await
            .or_already_exists(cr)
    }
    /// # Errors
    ///
    /// fail to convert kube-config into kube-client
    pub async fn await_crd(&self, name: &str) -> anyhow::Result<()> {
        let api = self.api_all::<CustomResourceDefinition>();
        for _ in 0..1000 {
            let crd = api
                .get_status(name)
                .await
                .map_err(|e| tracing::error!(name, ?e, "failed to get-status of crd"));
            if is_crd_established().matches_object(crd.as_ref().ok()) {
                return Ok(());
            }
            tracing::warn!(name, ?crd, "crd does not meet conditions");

            tokio::time::sleep(tokio::time::Duration::from_secs(5)).await;
        }

        anyhow::bail!("failed to get crd status 100 times for {}", name)
    }

    /// # Errors
    ///
    /// fail to create one of
    /// - deployment
    /// - service
    pub async fn apply(&self, config: &RuntimeConfig, spec: &Spec) -> anyhow::Result<()> {
        for config in as_config_map(spec) {
            self.api().apply(config).await?;
        }

        for secret in as_secret(config, spec) {
            self.api().apply(secret).await?;
        }

        match as_either_deployment_specs(&self.service_account, config, spec)? {
            EitherDeployment::Deployment(dep) => {
                self.api().apply(dep).await?;
            }
            EitherDeployment::StatefulSet(stf) => {
                self.api().apply(stf).await?;
            }
            EitherDeployment::DaemonSet(daemon) => {
                self.api().apply(daemon).await?;
            }
            EitherDeployment::Job(job) => create_job(&self.api().api, job).await?,
        };

        if let Some((service, ingress)) = as_service_spec(config, spec) {
            self.api().apply(service).await?;
            if let Some(ingress) = ingress {
                self.api().apply(ingress).await?;
            }
        }

        match spec.deployment {
            DeploymentKind::ReplicaSet { .. } => self.await_deployment(&config.name).await,
            DeploymentKind::StatefulSet { .. } => self.await_statefulset(&config.name).await,
            DeploymentKind::DaemonSet { .. } => self.await_daemonset(&config.name).await,
            DeploymentKind::Job { .. } => Ok(()),
        }
    }

    /// # Errors
    ///
    /// failed to delete one of
    /// - config-map
    /// - deployment
    ///     or its inner components - i.e. replica-set or pods etc..
    /// - service
    pub async fn delete(&self, config: &RuntimeConfig, spec: &Spec) -> anyhow::Result<()> {
        let name = config.name.as_str();
        tracing::info!("delete container {}", name);

        if let Some((_, ingress)) = as_service_spec(config, spec) {
            if ingress.is_some() {
                self.api::<Ingress>().delete(name).await?;
            }

            self.api::<corev1::Service>().delete(name).await?;
        }

        for config in as_config_map(spec) {
            let name = &config.name();
            self.api::<corev1::ConfigMap>().delete(name).await?;
        }

        self.deleted_deployment(name, &spec.deployment).await
    }

    pub async fn deleted_deployment(
        &self,
        name: &str,
        deployment: &DeploymentKind,
    ) -> anyhow::Result<()> {
        match deployment {
            DeploymentKind::ReplicaSet { .. } => {
                delete_and_await(self.api::<Deployment>().api, name).await?;
            }
            DeploymentKind::StatefulSet { .. } => {
                delete_and_await(self.api::<StatefulSet>().api, name).await?;
            }
            DeploymentKind::DaemonSet { .. } => {
                delete_and_await(self.api::<DaemonSet>().api, name).await?;
            }
            // no need to delete jobs - they are collected via ttl
            DeploymentKind::Job { .. } => {}
        }
        Ok(())
    }

    /// await sure the the container is ready before we continue to the next one
    /// Check deployment Conditions:
    ///   Type           Status  Reason
    ///   ----           ------  ------
    ///   Available      True    `MinimumReplicasAvailable`
    ///
    /// # Errors
    ///
    /// deployment was not `Available` and `True` for 500s
    pub async fn await_deployment(&self, name: &str) -> anyhow::Result<()> {
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        let api = self.api::<Deployment>();

        for _ in 0..100 {
            let conditions: Vec<_> = api
                .get_status(name)
                .await
                .map_err(|e| tracing::error!(name, ?e, "failed to get-status of deployment"))
                .ok()
                .and_then(|deployment| deployment.status)
                .into_iter()
                .filter_map(|status| status.conditions)
                .flatten()
                .collect();

            let ready = conditions
                .iter()
                .any(|condition| condition.type_ == "Available" && condition.status == "True");

            if ready {
                return Ok(());
            }
            tracing::warn!(name, ?conditions, "deployment does not meet conditions");

            tokio::time::sleep(tokio::time::Duration::from_secs(5)).await;
        }

        anyhow::bail!("failed to get deployment status 100 times for {}", name)
    }

    /// await sure the the container is ready before we continue to the next one
    /// Check deployment Conditions:
    ///   Type           Status  Reason
    ///   ----           ------  ------
    ///   Available      True    `MinimumReplicasAvailable`
    ///
    /// # Errors
    ///
    /// deployment was not `Available` and `True` for 500s
    pub async fn await_statefulset(&self, name: &str) -> anyhow::Result<()> {
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        let api = self.api::<StatefulSet>();

        for _ in 0..100 {
            let status = api
                .get_status(name)
                .await
                .map_err(|e| tracing::error!(name, ?e, "failed to get-status of stateful-set"))
                .ok()
                .and_then(|deployment| deployment.status);
            let ready = status.as_ref().map_or(false, |status| {
                status.ready_replicas == Some(status.replicas)
            });

            if ready {
                return Ok(());
            }
            tracing::warn!(name, ?status, "statefulset does not meet conditions");

            tokio::time::sleep(tokio::time::Duration::from_secs(5)).await;
        }

        anyhow::bail!("failed to get deployment status 100 times for {}", name)
    }

    pub async fn await_daemonset(&self, name: &str) -> anyhow::Result<()> {
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        let api = self.api::<DaemonSet>();
        for _ in 0..300 {
            let daemonset = api.get_status(name).await;
            let status = daemonset
                .map_err(|e| tracing::error!(name, ?e, "failed to get-status of daemon-set"))
                .ok()
                .and_then(|deployment| deployment.status);
            let ready = status.as_ref().map_or(false, |status| {
                status.desired_number_scheduled == status.number_ready
            });

            if ready {
                tracing::info!(name, ?status, "daemon-set ready");
                return Ok(());
            }

            tracing::warn!(name, ?status, "daemon-set does not meet conditions");

            tokio::time::sleep(tokio::time::Duration::from_secs(5)).await;
        }

        anyhow::bail!("failed to get daemon-set status 300 times for {}", name)
    }
}

impl Kubectl {
    /// # Errors
    ///
    /// service or it's ip was None for 100s
    pub async fn service_ip(&self, name: impl AsRef<str>) -> anyhow::Result<IpAddr> {
        let name = name.as_ref();
        let api = self.api::<corev1::Service>();
        for _ in 0..100 {
            let cluster_ip = api
                .get(name)
                .await
                .map_err(|e| tracing::error!(name, ?e, "failed to get-status of deployment"))
                .ok()
                .and_then(|service| service.spec?.cluster_ip);

            if let Some(ip) = cluster_ip {
                return ip.parse().context("failed to parse ip");
            }
            tracing::warn!(name, "service does not have cluster_ip");

            tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        }

        anyhow::bail!("failed to get deployment status 10 times for {}", name)
    }

    /// # Errors
    ///
    /// - `volume_name` is not found in `spec.container.config_volumes`
    /// - could not replace `config_map`
    pub async fn replace_config_volume(
        &self,
        spec: &Spec,
        volume_name: &str,
    ) -> anyhow::Result<()> {
        let volume = spec
            .config_volumes
            .iter()
            .find(|volume| volume.mount.name == volume_name)
            .with_context(|| format!("find volume {}", volume_name))?;
        let config_map = volume_as_config_map(volume);

        self.api().apply(config_map).await?;

        Ok(())
    }
}

pub fn write_account_yaml(
    mut w: impl std::io::Write,
    role_name: impl AsRef<str>,
    account_name: impl AsRef<str>,
    image_pull_secret: impl AsRef<str>,
    namespace: impl AsRef<str>,
) -> anyhow::Result<()> {
    let (service_account, role, role_binding) =
        account(role_name, account_name, image_pull_secret, namespace);

    to_writer(&mut w, &role).with_context(|| format!("serde {:?}", role))?;
    to_writer(&mut w, &service_account).with_context(|| format!("serde {:?}", service_account))?;
    to_writer(&mut w, &role_binding).with_context(|| format!("serde {:?}", role_binding))?;

    Ok(())
}

pub fn write_deployment_yaml(
    mut w: impl std::io::Write,
    namespace: &str,
    config: &RuntimeConfig,
    spec: &Spec,
) -> anyhow::Result<()> {
    for configmap in as_config_map(spec) {
        to_writer(&mut w, &configmap).with_context(|| format!("serde {:?}", configmap))?;
    }
    as_either_deployment_specs(namespace, config, spec)?.as_yaml(&mut w)?;

    if let Some((service, ingress)) = as_service_spec(config, spec) {
        to_writer(&mut w, &service).with_context(|| format!("serde {:?}", service))?;
        if let Some(ingress) = ingress {
            to_writer(&mut w, &ingress).with_context(|| format!("serde {:?}", ingress))?;
        }
    }

    Ok(())
}
