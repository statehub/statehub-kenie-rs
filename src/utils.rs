//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//
use std::collections::BTreeMap;
use std::fmt;
use std::fmt::Debug;

use crate::k8s::openapi::api::batch::v1::Job;
use anyhow::Context;
use either::Either;
use kube::core::response::Status;

pub(crate) trait WithOption: Sized {
    fn with_option(self) -> Option<Self>;
}
impl<T> WithOption for Vec<T> {
    fn with_option(self) -> Option<Self> {
        (!self.is_empty()).then(|| self)
    }
}

impl<K, V> WithOption for BTreeMap<K, V> {
    fn with_option(self) -> Option<Self> {
        (!self.is_empty()).then(|| self)
    }
}

pub(crate) trait TraceResult<T> {
    fn trace(self) -> Self;
}

impl<T: fmt::Debug + serde::Serialize, E: fmt::Debug> TraceResult<T> for Result<T, E> {
    fn trace(self) -> Self {
        match &self {
            Ok(item) => match serde_yaml::to_string(&item) {
                Ok(yaml) => tracing::info!(%yaml, "created"),
                Err(e) => tracing::error!(?item, ?e, "create"),
            },
            Err(e) => tracing::error!(?e, "create"),
        }
        self
    }
}

pub(crate) trait OrAlreadyExists<T> {
    fn or_already_exists(self, or_else: T) -> anyhow::Result<T>;
}
impl<T: fmt::Debug + serde::Serialize> OrAlreadyExists<T> for kube::Result<T> {
    fn or_already_exists(self, or_else: T) -> anyhow::Result<T> {
        use kube::Error::Api;
        let conflict = http::StatusCode::CONFLICT.as_u16();
        self.or_else(|e| match e {
            Api(e) if e.reason == "AlreadyExists" && e.code == conflict => Ok(or_else),
            e => Err(e).with_context(|| format!("create {:100?}", or_else)),
        })
        .trace()
    }
}

pub(crate) trait OrNotFound<T> {
    fn or_not_found(self) -> anyhow::Result<Either<T, Status>>;
}

impl<T: fmt::Debug + serde::Serialize> OrNotFound<T> for kube::Result<Either<T, Status>> {
    fn or_not_found(self) -> anyhow::Result<Either<T, Status>> {
        use kube::Error::Api;
        let not_found = http::StatusCode::NOT_FOUND.as_u16();

        let this = self.or_else(|e| match e {
            Api(e) if e.code == not_found => Ok(Either::Right(Status {
                status: e.status,
                message: e.message,
                reason: e.reason,
                details: None,
                code: e.code,
            })),
            e => Err(e.into()),
        });

        match &this {
            Ok(Either::Left(item)) => match serde_yaml::to_string(&item) {
                Ok(yaml) => tracing::info!(%yaml, "delete"),
                Err(e) => tracing::error!(?item, ?e, "delete"),
            },
            Ok(Either::Right(status)) => tracing::info!("delete: \n {:?}", status),
            Err(e) => tracing::error!(?e, "delete"),
        }

        this
    }
}

/// crate job and wait that it is completed successfully
pub async fn create_job(api: &kube::api::Api<Job>, job: Job) -> anyhow::Result<()> {
    let pp = kube::api::PostParams::default();
    let job: Job = api.create(&pp, &job).await.trace()?;
    if let Some(name) = &job.metadata.name {
        await_job(api, name).await?;
    } else {
        tracing::warn!(
            "job {:?} doesnt have a name in output, cannot await it",
            job.metadata.generate_name
        );
    }

    Ok(())
}

async fn await_job(api: &kube::api::Api<Job>, name: &str) -> anyhow::Result<()> {
    tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;

    for _ in 0..100 {
        let status = api
            .get_status(name)
            .await
            .map_err(|e| tracing::error!(name, ?e, "failed to get-status of job"))
            .ok()
            .and_then(|job| job.status);
        if let Some(status) = &status {
            tracing::info!(name, ?status, "job-status");
            let succeeded = status
                .succeeded
                .filter(|&succeeded| succeeded > 0)
                .is_some();
            let completion = status.completion_time.is_some();
            if succeeded & completion {
                return Ok(());
            }
        }
        tracing::warn!(name, ?status, "job does not meet conditions");

        tokio::time::sleep(tokio::time::Duration::from_secs(5)).await;
    }

    anyhow::bail!("failed to get deployment status 100 times for {}", name)
}

/// delete resource by `name` of type `K`
/// and await that it is deleted, i.e. get-status code is `404`
pub(crate) async fn delete_and_await<K>(api: kube::api::Api<K>, name: &str) -> anyhow::Result<()>
where
    K: Clone
        + Send
        + Debug
        + kube::core::Resource
        + serde::de::DeserializeOwned
        + serde::Serialize
        + 'static,
{
    use kube::runtime::wait;
    use wait::delete::delete_and_finalize;
    use wait::delete::Error::Delete;
    let not_found = http::StatusCode::NOT_FOUND.as_u16();

    let dp = &kube::api::DeleteParams {
        propagation_policy: Some(kube::api::PropagationPolicy::Foreground),
        ..kube::api::DeleteParams::default()
    };

    delete_and_finalize::<K>(api, name, dp).await.or_else(|e| {
        matches!(&e, Delete(kube::Error::Api(e)) if e.code == not_found)
            .then(|| ())
            .ok_or(e)
    })?;
    Ok(())
}
