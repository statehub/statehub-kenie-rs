#[cfg(kubectl)]
#[tokio::main]
async fn main() {
    use statehub_kenie as kenie;
    use std::collections::HashMap;

    use maplit::{convert_args, hashmap};

    let spec = kenie::spec::Spec {
        secrets: convert_args!(
            hashmap!("test-secret" => convert_args!(hashmap!("SOME_ENV_VARIABLE" => "YO")))
        ),
        commands: vec!["/bin/echo".into()],
        args: vec!["$(SOME_ENV_VARIABLE)".into()],
        ..kenie::spec::Spec::default()
    };
    let config = kenie::spec::RuntimeConfig {
        name: "testing".into(),
        image: "debian".into(),
        environment: HashMap::new().into(),
        owner_reference: None,
    };
    let client = kube::Client::try_default().await.unwrap();
    let client = kenie::kubectl::Kubectl::with_client(
        client,
        "default".to_string(),
        "pls".to_string(),
        None,
    );
    client.apply(&config, &spec).await.unwrap();
}

#[cfg(not(kubectl))]
fn main() {
    panic!("cfg(kubectl) != true")
}
