function() {
  local concourse = import '../../statehub-concourse/jsonnet/concourse.libsonnet',

  local resource_types = [
    concourse.MergeRequestResourceType(),
  ],

  local kind_config = |||
    kind: Cluster
    apiVersion: kind.x-k8s.io/v1alpha4
    nodes:
    - role: control-plane
  |||,

  local resources = [
    concourse.GitResource('kind-on-c', 'https://github.com/pivotal-k8s/kind-on-c', branch='master'),
    {
      name: 'kind-release',
      type: 'github-release',
      source: {
        owner: 'kubernetes-sigs',
        repository: 'kind',
        pre_release: false,
      },
    },
    concourse.DockerResource('node_image', 'kindest/node', tag='v1.23.4'),
    concourse.DockerResource('kindoncrust', 'registry.gitlab.com/replixio/images/kindonc', 'default', auth=true),
    concourse.MergeRequestResource('kenie-mr', 'https://gitlab.com/statehub/statehub-kenie-rs', 'develop', skip_clone=false),
  ],

  local jobs = [
    concourse.Job(
      'kind',
      toReport=false,
      serial=false,
      plan=[
        concourse.Parallel([
          concourse.Get('kenie-mr', trigger=true),
          concourse.Get('kind-on-c'),
          concourse.Get('node_image'),
          concourse.Get('kindoncrust'),
          concourse.Get('kind-release', params={
            globs: ['kind-linux-amd64'],
          }),
        ]),
        concourse.MergeRequestRunning('kenie-mr'),
        concourse.Task(
          'run-kind',
          privileged=true,
          file='kind-on-c/kind.yaml',
          params={
            KIND_TESTS: |||
              cd inputs
              cargo version
              rustc --version
              cargo test
              cargo test --all-features -- --ignored
            |||,
            KIND_CONFIG: kind_config,
          },
          input_mapping={
            node_image: 'node_image',
            inputs: 'kenie-mr',
          },
          image='kindoncrust',
        ),
        concourse.MergeRequestSuccess('kenie-mr'),
      ],
      on_failure=[concourse.MergeRequestFailed('kenie-mr')],
      on_error=[concourse.MergeRequestFailed('kenie-mr')]
    ),
  ],

  resource_types: resource_types,
  resources: resources,
  jobs: jobs,
}
